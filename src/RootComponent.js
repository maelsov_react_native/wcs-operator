import React, { Component } from 'react';
import { StatusBar, Platform, View } from 'react-native';
import NavigationRouter from './navigations/NavigationRouter'
import multiTheme from './multiTheme';
import { connect } from 'react-redux';

class RootComponent extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar backgroundColor={multiTheme[this.props.chosenTheme].backgroundColor} barStyle={Platform.OS == 'android' ? multiTheme[this.props.chosenTheme].barStyle : 'dark-content'} />
        <NavigationRouter />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    chosenLanguage: state.counterOperation.language,
    chosenTheme: state.counterOperation.theme
  };
};

export default connect(mapStateToProps)(RootComponent);