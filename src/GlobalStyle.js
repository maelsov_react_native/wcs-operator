import {
    StyleSheet,
    Platform,
    Dimensions
} from 'react-native';
import { Neo, NeoLight, NeoMedium, WhiteColor, GrayColor } from './GlobalConfig'
const { width, height } = Dimensions.get('window')

const GlobalStyle = StyleSheet.create({
    modalTitle: {
        fontSize: 16,
        textAlign: "center",
        fontWeight: "bold",
        color: GrayColor,
        fontFamily: 'NeoSansStd-Medium',
        marginVertical: 10

    },
    modalStyle: {
        backgroundColor: "#fff",
        padding: 20,
        borderRadius: 8
    },
    containerFlexRow: {
        marginVertical: 10,
        flexDirection: "row",
        justifyContent: "center"
    },
    modalYaButton: {
        height: 43,
        backgroundColor: "#787878",
        marginRight: 10,
        flex: 0.5,
        borderRadius: 8
    },
    modalTidakButton: {
        height: 43,
        backgroundColor: "#E52A34",
        marginRight: 10,
        flex: 0.5,
        borderRadius: 8
    },
    closeIcon: {
        paddingHorizontal: 20,
        alignItems: 'center'
    },
    buttonModalText: {
        color: "#fff",
        fontWeight: "bold",
        fontSize: 14,
        fontFamily: 'NeoSansStd-Medium',

    },
    container: {
        flex: 1,
        backgroundColor: WhiteColor
    },
    containerDark: {
        flex: 1,
        backgroundColor: '#2d2d2d',
    },
    newsCard: {
        marginBottom: 20,
        paddingHorizontal: 20
    },
    imageBackgroundStyle: {
        width: '100%',
        aspectRatio: 1.85,
        resizeMode: 'cover',
        backgroundColor: WhiteColor,
        overflow: 'hidden',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
    },
    imageStyle: {
        borderRadius: 10
    },
    bookmarkContent: {
        position: 'absolute',
        flexDirection: 'row',
        flexWrap: 'wrap',
        top: 0,
        right: 6,
        marginTop: 6,
        borderRadius: 30,
        backgroundColor: '#ecf0f1',
        paddingHorizontal: 8,
        paddingTop: 8,
        paddingBottom: 6,
        justifyContent: 'center',
        alignItems: 'center'
    },
    tagContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        position: 'absolute',
        bottom: 0,
        left: 6
    },
    tagContent: {
        borderRadius: 10,
        backgroundColor: 'rgba(236, 240, 241,0.8)',
        paddingHorizontal: 8,
        paddingTop: 8,
        paddingBottom: 6,
        marginRight: 6,
        marginBottom: 6
    },
    tagContentDark: {
        borderRadius: 10,
        backgroundColor: '#000',
        paddingHorizontal: 8,
        paddingTop: 8,
        paddingBottom: 6,
        marginRight: 6,
        marginBottom: 6
    },
    tagText: {
        fontFamily: Neo,
        fontSize: 10
    },
    tagTextDark: {
        fontFamily: Neo,
        color: '#fff',
        opacity: 0.7,
        fontSize: 8,
        textAlignVertical: "center",
        textAlign: "center"
    },
    cardTitleText: {
        fontFamily: NeoMedium,
        fontSize: 16,
        marginTop: 14,
        marginBottom: 4,
        color: '#000',
        letterSpacing: 1
    },
    cardTitleTextDark: {
        fontFamily: NeoMedium,
        fontSize: 16,
        marginTop: 14,
        marginBottom: 4,
        color: '#fff',
        letterSpacing: 1
    },
    cardSubtitleText: {
        fontFamily: Neo,
        fontSize: 12,
        color: '#787878',
    },
    headerStyle: {
        backgroundColor: '#ffffff',
        elevation: 2,
        height: 50
    },
    headerStyleDark: {
        backgroundColor: '#000',
        elevation: 2,
        height: 50
    },
    logo: {
        width: 200,
        height: '80%',
        resizeMode: 'contain'
    },
    button: {
        flex: 1,
        borderRadius: 5,
        backgroundColor: '#787878',
        // backgroundColor: '#5ba382',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        fontFamily: NeoMedium,
        color: '#fff'
    }
});

export default GlobalStyle;