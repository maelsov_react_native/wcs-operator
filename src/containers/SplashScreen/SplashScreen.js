import React, { Component } from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';
import GlobalStyle from '../../GlobalStyle'
import { connect } from 'react-redux';
import multiTheme from '../../multiTheme'
import { Actions } from 'react-native-router-flux';
import { wait } from '../../GlobalFunction';
import { NeoMedium, BlackColor } from '../../GlobalConfig';
import AsyncStorage from '@react-native-community/async-storage'

class SplashScreen extends Component {
	componentDidMount() {
		AsyncStorage.getItem('user')
			.then(res => {
				if (res) {
					wait(2000).then(() => Actions.push('eventList'))
				}
				else {
					wait(2000).then(() => Actions.push('login'))
				}
			})
			.catch(err => {
				Actions.push('login')
			})
	}

	render() {
		return (
			<View style={[GlobalStyle.container, { justifyContent: 'center', alignItems: 'center', backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor }]}>
				<Image resizeMode='contain' source={require('../../images/logo.png')} style={[styles.logo, { tintColor: multiTheme[this.props.chosenTheme].tintColor }]} />
				<Text style={{ fontFamily: NeoMedium, fontSize: 16, color: BlackColor }}>Mobile Operator App</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		justifyContent: 'center',
		alignItems: 'center'
	},
	logo: {
		width: 150,
		height: 150
	}
});

const mapStateToProps = state => {
	return {
		chosenLanguage: state.counterOperation.language,
		chosenTheme: state.counterOperation.theme
	};
};


export default connect(mapStateToProps)(SplashScreen);
