import {
    StyleSheet,
    Platform,
    Dimensions
} from 'react-native';
import { Neo, NeoLight, NeoMedium } from '../../GlobalConfig'

const { width, height } = Dimensions.get('window')


const GlobalStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff'
    },
    label: {
        // fontFamily: Neo,
        fontSize: 14,
        color: '#000'
    },
    labelDark: {
        // fontFamily: Neo,
        fontSize: 14,
        color: '#fff'
    },
    button: {
		width: '90%',
		backgroundColor: '#787878',
        paddingVertical: 20,
        borderRadius: 10,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonText: {
		fontFamily: 'NeoSansStd-Medium',
		color: '#fff'
	}
});

export default GlobalStyle;