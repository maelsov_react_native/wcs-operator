import React, { Component } from 'react';
import { Platform, View, Text, Image, ActivityIndicator } from 'react-native';
import { Container, Content, Input, Button } from 'native-base';
import { Actions } from 'react-native-router-flux'
import GlobalStyle from '../../GlobalStyle'
import multiLanguageText from './LanguageText'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import styles from './styles'
import { GrayColor, BlackColor, NeoMedium, LightGrayColor, WhiteColor } from '../../GlobalConfig';
import AsyncStorage from '@react-native-community/async-storage'
import { modelUser } from '../../models/User';
import { wait } from '../../GlobalFunction';
import { connect } from 'react-redux';
import multiTheme from '../../multiTheme'
import CustomToast from '../../components/CustomToast';
import CustomTextInputWithIcon from '../../components/CustomTextInputWithIcon';
import CustomButton from '../../components/CustomButton';

class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            // email: 'wiryadinata.aldebaran921@gmail.com',
            // password: 'Password1*',
            isLoading: false,
            isModalVisible: false
        }
    }

    componentDidMount() {
        if (this.props.updateLogout) {
            this.toast.ShowToastFunction('success', 'Logout success!')
        }
    }

    handleInputChange = (field) => (value) => this.setState({ [field]: value })

    handleLogin = () => {
        if (!this.state.email && !this.state.password) this.toast.ShowToastFunction('warning', multiLanguageText[this.props.chosenLanguage].warn_usernamePassword)
        else if (!this.state.email) this.toast.ShowToastFunction('warning', multiLanguageText[this.props.chosenLanguage].warn_email)
        else if (!this.state.password) this.toast.ShowToastFunction('warning', multiLanguageText[this.props.chosenLanguage].warn_password)
        else {
            this.setState({ isLoading: true })
            const formdata = new FormData
            formdata.append('email', this.state.email)
            formdata.append('password', this.state.password)
            formdata.append('is_mobile_operator', '1')

            modelUser.login(formdata,
                res => {
                    console.log(res)
                    if (res.status == 200) {
                        AsyncStorage.setItem('user', JSON.stringify(res.result.data))
                        AsyncStorage.setItem('token', res.result.data.token)
                        this.toast.ShowToastFunction('success', multiLanguageText[this.props.chosenLanguage].toastLogin)
                        wait(1000).then(() => {
                            Actions.eventList({ type: 'reset' })
                            this.setState({ isLoading: false })
                        })
                    }
                    else if (res.status == 400) {
                        switch (res.result.code) {
                            case "auth_login_error_1":
                                this.toast.ShowToastFunction('warning', multiLanguageText[this.props.chosenLanguage].toastUserExpired)
                                break;
                            case "auth_login_error_2":
                            case "auth_login_error_3":
                            case "auth_login_error_4":
                                this.toast.ShowToastFunction('warning', multiLanguageText[this.props.chosenLanguage].toastNotLogin)
                                break;
                            case "auth_login_error_5":
                                this.toast.ShowToastFunction('warning', multiLanguageText[this.props.chosenLanguage].toastUserNotExist)
                                break;
                            default:
                                break;
                        }
                        this.setState({ isLoading: false })
                    }
                    else {
                        this.toast.ShowToastFunction('warning', multiLanguageText[this.props.chosenLanguage].toastUserNotExist)
                        this.setState({ isLoading: false })
                    }
                }
            )
                .catch(err => {
                    console.log(err)
                    this.setState({ isLoading: false })
                })
        }
    }

    render() {
        return (
            <Container style={[GlobalStyle.container, { backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor }]}>
                <Content keyboardShouldPersistTaps='handled' contentContainerStyle={{ flex: 1 }}>
                    <View style={{ width: '100%', height: '30%', justifyContent: 'center', alignItems: 'center' }} >
                        <Image resizeMode='contain' source={require('../../images/logo.png')} style={{ width: '50%', height: '50%', tintColor: multiTheme[this.props.chosenTheme].tintColor }} />
                    </View>
                    <Text style={{ fontFamily: NeoMedium, fontSize: 16, color: BlackColor, alignSelf: 'center' }}>Mobile Operator App</Text>
                    <View style={{ flex: 1, width: '80%', alignSelf: 'center', alignItems: 'center', marginTop: 30 }}>
                        <CustomTextInputWithIcon
                            inputIcon='email'
                            isMaterialIcon
                            disabled={this.state.isLoading}
                            autoCapitalize="none"
                            keyboardType='email-address'
                            placeholder={multiLanguageText[this.props.chosenLanguage].placeholder_email}
                            chosenTheme={this.props.chosenTheme}
                            value={this.state.email}
                            onChangeText={this.handleInputChange("email")}
                            returnKeyType='next'
                            onSubmitEditing={() => this.passwordInput.focus()}
                        />
                        <CustomTextInputWithIcon
                            inputIcon='lock'
                            isMaterialIcon
                            disabled={this.state.isLoading}
                            autoCapitalize="none"
                            placeholder={multiLanguageText[this.props.chosenLanguage].placeholder_password}
                            chosenTheme={this.props.chosenTheme}
                            value={this.state.password}
                            onChangeText={this.handleInputChange("password")}
                            isPassword
                            withEye
                            ref={refs => { this.passwordInput = refs }}
                        />
                        <View style={{ marginTop: 30 }}>
                            <CustomButton
                                onPress={this.handleLogin}
                                disabled={this.state.isLoading}
                                label={multiLanguageText[this.props.chosenLanguage].login}
                            />
                        </View>
                    </View>
                </Content>
                <CustomToast ref={(ref) => { this.toast = ref; }} />
            </Container>
        );
    }


}
const mapStateToProps = state => {
    return {
        chosenLanguage: state.counterOperation.language,
        chosenTheme: state.counterOperation.theme
    };
};


export default connect(mapStateToProps)(LoginScreen);