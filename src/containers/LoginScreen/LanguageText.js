const multiLanguageText = {
	EN: {
		password: 'Password',
		login: 'Login',
		loginWithLinkedIn: 'Login with LinkedIn',
		register: 'Register now',
		modalLogin: 'Loading..',
		input_user_name: 'User Name',
		warn_email: 'Please input email!',
		warn_usernamePassword: 'Please input email and password!',
		warn_password: 'Please input password!',
		toastLogin: 'Login successfully!',
		toastUserExpired: 'Operator access already expired, please renew to login',
		toastNotLogin: 'Incorrect email or password!',
		toastUserNotExist: 'Email is not registered!',
		Email: 'Email Address',
		placeholder_password: 'Password',
		placeholder_email: 'Email address',


	},
	ID: {
		password: 'Kata Sandi',
		login: 'Masuk',
		loginWithLinkedIn: 'Masuk dengan LinkedIn',
		register: 'Daftar sekarang',
		input_user_name: 'Nama Pengguna',
		modalLogin: 'Memuat..',
		warn_email: 'Tolong masukan email!',
		warn_usernamePassword: 'Masukan email dan kata sandi!',
		warn_password: 'Masukan Kata sandi!',
		toastLogin: 'Login berhasil!',
		toastUserExpired: 'Akses operator telah kadaluarsa, mohon perbaharui untuk masuk',
		toastNotLogin: 'Email atau kata sandi salah!',
		toastUserNotExist: 'Email belum terdaftar!',
		Email: 'Alamat Email',
		placeholder_password: 'Kata sandi',
		placeholder_email: 'Alamat email',
	}
}

export default multiLanguageText