import React, { PureComponent } from 'react';
import {
	StyleSheet,
	Platform,
	TouchableOpacity,
	View,
	StatusBar
} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import { Actions } from 'react-native-router-flux';
import { GrayColor, WhiteColor } from '../../GlobalConfig';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';

const styles = StyleSheet.create({
	imageContainer: {
		flex: 1,
		backgroundColor: '#000',
		justifyContent: 'center'
	},
});

export default class ImageScreen extends PureComponent {
	render() {
		return (
			<View style={styles.imageContainer}>
				<View style={{
					height: 60,
					width: '100%',
					alignItems: 'center',
					flexDirection: 'row',
					position: 'absolute',
					top: StatusBar.currentHeight > 24 ? 40 : 20,
					zIndex: 1,
				}}>
					<TouchableOpacity onPress={() => Actions.pop()} style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center' }}>
						<View style={{ height: 30, width: 30, borderRadius: 15, overflow: 'hidden' }}>
							<View style={{ opacity: 0.5, backgroundColor: GrayColor, height: '100%', width: '100%', position: 'absolute' }} />
							<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
								{Platform.OS == 'android' ? <Feather name="arrow-left" color={WhiteColor} size={20} /> :
									<FontAwesome name="angle-left" color={WhiteColor} size={20} />}
							</View>
						</View>
					</TouchableOpacity>
				</View>
				<ImageViewer
					imageUrls={[
						{
							url: this.props.image,
							width: '100%',
							props: {
								resizeMode: 'contain'
							}
						}
					]}
					maxOverflow={0}
					saveToLocalByLongPress={false}
					renderIndicator={() => null}
				/>
			</View>
		);
	}
}