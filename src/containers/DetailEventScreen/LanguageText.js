const multiLanguageText = {
	EN: {
		loginModal: 'You need to login',
		buttonLogin: 'Login',
		toastSuccess: 'Delete bookmark successfully!',
		toastError: 'Delete bookmark not successfully!',
		modalTitle: 'Are you sure you want to delete bookmark?',
		modalButtonYes: 'Yes',
		modalButtonNo: 'No',
		ActivityIndicator: 'Loading....',
		registered: 'Registered',
		notRegistered: 'Register Event',
		downloadAttach: 'Download Attachment'
	},
	ID: {
		loginModal: 'Harap login untuk mengakses fitur ini',
		buttonLogin: 'Masuk',
		modalButtonYes: 'Yakin',
		modalButtonNo: 'Tidak',
		toastSuccess: 'Bookmark berhasil dihapus!',
		toastError: 'Bookmark gagal dihapus!',
		ActivityIndicator: 'Memuat....',
		modalTitle: 'Apakah Anda yakin untuk menghapus bookmark?',
		registered: 'Terdaftar',
		notRegistered: 'Daftar Acara',
		downloadAttach: 'Unduh Lampiran'

	}
}

export default multiLanguageText