import React, { Component } from 'react';
import {
	Text,
	View,
	Image,
	FlatList,
	Dimensions,
	ScrollView,
	TouchableOpacity,
	Linking,
	ActivityIndicator
} from 'react-native';
import { Content, Container, Tabs, Tab, TabHeading } from 'native-base';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import GlobalStyle from '../../GlobalStyle'
import { GrayColor, NeoMedium, LinkedInColor, SafeArea, LightGrayColor, WhiteColor, BlackColor, Images, Metrics } from '../../GlobalConfig';
import AutoHeightImage from 'react-native-auto-height-image';
import { Actions } from 'react-native-router-flux';
import { modelEvent } from '../../models/Event';
import { connect } from 'react-redux';
import multiTheme from '../../multiTheme'
import { FloatingAction } from "react-native-floating-action";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { wait, convertTime, convertDate } from '../../GlobalFunction';

const { width, height } = Dimensions.get('screen')

class DetailEventScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			data: [],
			event_img_schedule: 'https://i.pinimg.com/originals/c8/b2/05/c8b205edef3c9c5ab17a550ef9c63549.jpg',
			speakersData: [],
			isImageViewVisible: false,
			event_date_from: '',
			event_date_to: ''
		}
	}

	componentDidMount = () => {
		this.fetchEvent()
	}

	linkedTo(param) {
		Linking.canOpenURL(param)
			.then((supported) => {
				if (!supported) {
					console.log("Can't handle url: " + param);
				} else {
					return Linking.openURL(param);
				}
			})
			.catch((err) => console.error('An error occurred', err));
	}

	fetchEvent = () => {
		modelEvent.getEventDetail(this.props.eventID, res => {
			console.log(res)
			console.log(res.result.data.header.event_img_schedule)
			this.setState({
				data: res.result.data.header,
				event_date_from: convertDate(res.result.data.header.event_date_from),
				event_date_to: convertDate(res.result.data.header.event_date_to),
				speakersData: res.result.data.speakers,
				event_img_schedule: res.result.data.header.event_img_schedule,
				isLoading: false
			})
		})
			.catch(err => {
				console.log(err)
				this.setState({ isLoading: false })
			})
	}

	openScanMenu = () => {
		this.setState({
			isLoading: true
		}, () => wait(100).then(() => this.setState({ isLoading: false })))

		Actions.scanMenu({
			eventItem: this.state.data,
			user_id: this.state.user_id
		})
	}

	/**
	 * Generate Tab Heading
	 * Accept Tabs Identifier
	 */
	generateHeading = (headType) => {
		let type, count
		if (headType === 'detail') {
			// type = 'Detail'
			type = 'Detail'
		}
		else if (headType === 'speakers') {
			// type = 'Pembicara'
			type = 'Speakers'
			count = this.state.speakersData.length
		}
		else if (headType === 'schedule') {
			// type = 'Jadwal'
			type = 'Schedule'
		}
		return (
			<TabHeading style={{ backgroundColor: multiTheme[this.props.chosenTheme].white_blackLight }}>
				<Text style={{ fontWeight: 'bold', color: multiTheme[this.props.chosenTheme].fontColor }}>{type}</Text>
				{count ?
					<View style={{ height: 20, width: 20, borderRadius: 10, backgroundColor: '#000', justifyContent: 'center', alignItems: 'center', marginLeft: 5 }}>
						<Text style={{ fontWeight: 'bold', color: '#fff', fontSize: 12 }}>{count}</Text>
					</View> : null}
			</TabHeading>
		)
	}

	showAttendee = ({ item, index }) => {
		return (
			<View style={{ width: '100%', height: 70, flexDirection: 'row', paddingVertical: 5, paddingHorizontal: 20, marginBottom: 10, borderBottomWidth: 1, borderBottomColor: LightGrayColor }} key={index}>
				<View style={{ height: '100%', width: 70, paddingRight: 20, justifyContent: 'center', alignItems: 'center' }}>
					<Image style={{ height: 50, width: 50, borderRadius: 25 }} source={{ uri: item.attendee_photo }} />
				</View>
				<View style={{ flex: 1, justifyContent: 'center' }}>
					<Text numberOfLines={1} style={{ fontFamily: NeoMedium, color: '#000' }}>{item.attendee_name}</Text>
					<Text numberOfLines={1} style={{ fontSize: 12 }}>{item.attendee_email}</Text>
					<Text numberOfLines={1} style={{ fontSize: 12, color: '#000' }}>{item.attendee_company}</Text>
				</View>
			</View>
		)
	}

	renderSpeakers = ({ item, index }) => {
		return (
			<View style={{ width: '100%', flexDirection: 'row', marginBottom: 20, paddingBottom: 20, borderBottomColor: LightGrayColor, borderBottomWidth: 1, paddingHorizontal: SafeArea }}>
				<View style={{ height: 30, justifyContent: 'center', borderRightWidth: 2, borderRightColor: GrayColor, paddingRight: 10 }}>
					<Text style={{ fontSize: 24, color: multiTheme[this.props.chosenTheme].fontColor }}>{index + 1}</Text>
				</View>
				<View style={{ flex: 1, marginLeft: 10, justifyContent: 'center' }}>
					<Text style={{ fontFamily: NeoMedium, color: multiTheme[this.props.chosenTheme].fontColor }}>{item.event_speaker_fullname}</Text>
					<Text style={{ fontSize: 14, color: GrayColor }}>{item.event_speaker_department} of {item.event_speaker_institution}</Text>
					<TouchableOpacity onPress={() => this.linkedTo(item.event_speaker_linkedin_url)}>
						<Text style={{ fontFamily: NeoMedium, color: LinkedInColor, textAlign: 'right', marginTop: 5, fontSize: 12 }}>
							View LinkedIn Profile
						</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}

	onImageView = () => {
		Actions.image({ image: this.state.data.event_img_primary })
	}

	onScheduleView = () => {
		Actions.image({ image: this.state.event_img_schedule })
	}

	render() {
		return (
			<Container style={[GlobalStyle.container, { backgroundColor: multiTheme[this.props.chosenTheme].subBGHeadingColor }]}>
				<Tabs
					style={{ flex: 1 }}
					tabBarBackgroundColor={WhiteColor}
					prerenderingSiblingsNumber={2}
					tabContainerStyle={{ overflow: 'hidden', height: 50, backgroundColor: WhiteColor }}
					tabBarUnderlineStyle={{ backgroundColor: multiTheme[this.props.chosenTheme].fontColor, borderRadius: 10, marginBottom: -3, height: 6 }}
				>
					<Tab heading={this.generateHeading('detail')}>
						<Content
							contentContainerStyle={{ paddingBottom: 100 }}
							style={{ width: '100%', backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor }}>
							<TouchableOpacity onPress={this.onImageView} style={{ width: '100%', height: Metrics.SCREEN_WIDTH / 1.5, marginBottom: 20 }}>
								<Image
									source={{ uri: this.state.data.event_img_primary }}
									style={{ flex: 1, marginBottom: 20 }}
									defaultSource={Images.PLACEHOLDER}
									resizeMode='cover' />
							</TouchableOpacity>
							<View style={{ width: '100%', flexDirection: 'row', marginBottom: 20, paddingHorizontal: SafeArea }}>
								<View style={{ height: 30, width: 40, justifyContent: 'center', alignItems: 'center', borderRightWidth: 2, borderRightColor: GrayColor, paddingHorizontal: 5 }}>
									<SimpleLineIcons
										size={25}
										color={GrayColor}
										name={`event`}
									/>
								</View>
								<View style={{ flex: 1, marginLeft: 10 }}>
									<Text style={{ fontFamily: NeoMedium, color: multiTheme[this.props.chosenTheme].fontColor }}>Title</Text>
									<Text style={{ fontSize: 16, color: multiTheme[this.props.chosenTheme].fontColor }}>{this.state.data.event_title}</Text>
								</View>
							</View>
							<View style={{ width: '100%', flexDirection: 'row', marginBottom: 20, paddingHorizontal: SafeArea }}>
								<View style={{ height: 30, width: 40, justifyContent: 'center', alignItems: 'center', borderRightWidth: 2, borderRightColor: GrayColor, paddingHorizontal: 5 }}>
									<FontAwesome
										size={25}
										color={GrayColor}
										name={`calendar`}
									/>
								</View>
								<View style={{ flex: 1, marginLeft: 10 }}>
									<Text style={{ fontFamily: NeoMedium, color: multiTheme[this.props.chosenTheme].fontColor }}>Date</Text>
									<Text style={{ fontSize: 16, color: multiTheme[this.props.chosenTheme].fontColor }}>
										{this.state.event_date_from != this.state.event_date_to ? `${this.state.event_date_from} - ${this.state.event_date_to}` : this.state.event_date_from}
									</Text>
								</View>
							</View>
							<View style={{ width: '100%', flexDirection: 'row', marginBottom: 20, paddingHorizontal: SafeArea }}>
								<View style={{ height: 30, width: 40, justifyContent: 'center', alignItems: 'center', borderRightWidth: 2, borderRightColor: GrayColor, paddingHorizontal: 5 }}>
									<SimpleLineIcons
										size={25}
										color={GrayColor}
										name={`clock`}
									/>
								</View>
								<View style={{ flex: 1, marginLeft: 10 }}>
									<Text style={{ fontFamily: NeoMedium, color: multiTheme[this.props.chosenTheme].fontColor }}>Time</Text>
									<Text style={{ fontSize: 16, color: multiTheme[this.props.chosenTheme].fontColor }}>{this.state.data.event_time_from ? convertTime(this.state.data.event_time_from) : ''} - {this.state.data.event_time_to ? convertTime(this.state.data.event_time_to) : ''}</Text>
								</View>
							</View>
							<View style={{ width: '100%', flexDirection: 'row', marginBottom: 20, paddingHorizontal: SafeArea }}>
								<View style={{ height: 30, width: 40, justifyContent: 'center', alignItems: 'center', borderRightWidth: 2, borderRightColor: GrayColor, paddingHorizontal: 5 }}>
									<FontAwesome
										size={25}
										color={GrayColor}
										name={`map-marker`}
									/>
								</View>
								<View style={{ flex: 1, marginLeft: 10 }}>
									<Text style={{ fontFamily: NeoMedium, color: multiTheme[this.props.chosenTheme].fontColor }}>Location</Text>
									<Text style={{ color: multiTheme[this.props.chosenTheme].fontColor }}>{this.state.data.event_address ? `${this.state.data.event_address}, ${this.state.data.event_district}, ${this.state.data.event_regency}, ${this.state.data.event_province}, ${this.state.data.event_country}` : ''}</Text>
								</View>
							</View>
						</Content>
					</Tab>
					<Tab heading={this.generateHeading('speakers')}>
						<FlatList
							style={{ paddingVertical: SafeArea, backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor }}
							alwaysBounceVertical
							keyExtractor={item => item.event_speaker_id}
							data={this.state.speakersData}
							extraData={this.state.speakersData}
							renderItem={this.renderSpeakers}
							ListEmptyComponent={(
								<View style={[GlobalStyle.container, { flexDirection: 'row', justifyContent: 'center', backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor }]}>
									<View style={[{ height: '100%', alignItems: 'center', justifyContent: 'center' }]}>
										<Text style={[{ fontFamily: NeoMedium, fontSize: 15, marginVertical: 10, textAlign: 'center', color: multiTheme[this.props.chosenTheme].fontColor }]}>No Speaker</Text>
									</View>
								</View>
							)}
						/>
					</Tab>

					<Tab heading={this.generateHeading('schedule')}>
						{this.state.event_img_schedule ? (
							<TouchableOpacity onPress={this.onScheduleView}>
								<ScrollView>
									<AutoHeightImage
										width={width}
										defaultSource={Images.PLACEHOLDER}
										resizeMode='contain'
										source={{ uri: this.state.event_img_schedule }}
									/>
								</ScrollView>
							</TouchableOpacity>
						) : (
								<View style={[GlobalStyle.container, { flexDirection: 'row', justifyContent: 'center', backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor }]}>
									<View style={[{ height: '100%', alignItems: 'center', justifyContent: 'center' }]}>
										<Text style={[{ fontFamily: NeoMedium, fontSize: 15, marginVertical: 10, textAlign: 'center', color: multiTheme[this.props.chosenTheme].fontColor }]}>No Schedule</Text>
									</View>
								</View>
							)}
					</Tab>
				</Tabs>
				<FloatingAction
					color={BlackColor}
					showBackground={false}
					onPressMain={!this.state.isLoading ? this.openScanMenu : null}
					floatingIcon={this.state.isLoading ? (
						<ActivityIndicator color={WhiteColor} />
					) : <MaterialCommunityIcons color={WhiteColor} size={24} name='qrcode-scan' />}
				/>
			</Container>
		);
	}
}
const mapStateToProps = state => {
	return {
		chosenLanguage: state.counterOperation.language,
		chosenTheme: state.counterOperation.theme
	};
};

export default connect(mapStateToProps)(DetailEventScreen);