import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert, ActivityIndicator } from 'react-native';
import GlobalStyle from '../../GlobalStyle'
import { connect } from 'react-redux';
import multiTheme from '../../multiTheme'
import { Actions } from 'react-native-router-flux';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import AsyncStorage from '@react-native-community/async-storage'
import { NeoMedium, Neo, SafeArea, GrayColor, LightGrayColor, GreenColor, WhiteColor } from '../../GlobalConfig';
import multiLanguageText from './LanguageText';
import { modelUser } from '../../models/User';
import CustomToast from '../../components/CustomToast';

class SettingScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user_id: '',
			user_fullname: '',
			user_email: '',
			isLoading: false
		}
	}

	componentDidMount() {
		AsyncStorage.getItem('user')
			.then(data => {
				if (data) {
					const parsedData = JSON.parse(data)
					this.setState({
						user_id: parsedData.user_data.user_id,
						user_fullname: parsedData.user_data.user_fullname,
						user_email: parsedData.user_data.user_email
					})
				}
			})
	}

	handleLogout = () => {
		Alert.alert(
			'Confirm Logout',
			'Do you want to logout?',
			[
				{
					text: 'Cancel',
					onPress: () => console.log('Cancel Pressed'),
					style: 'cancel',
				},
				{
					text: 'Logout', onPress: () => {
						this.setState({ isLoading: true })
						const formdata = new FormData
						formdata.append('user_id', this.state.user_id)
						modelUser.logout(formdata, res => {
							console.log(res)
							if (res.status == 200) {
								AsyncStorage.removeItem('user')
								AsyncStorage.removeItem('token')
								Actions.push('login', {
									updateLogout: true
								})
							}
							else {
								this.toast.ShowToastFunction('warning', 'Logout failed, please try again later')
							}
							this.setState({ isLoading: false })
						})
							.catch(err => {
								this.setState({ isLoading: false })
								this.toast.ShowToastFunction('warning', 'Logout failed, please try again later')
								console.log(err)
							})
					}
				},
			],
			{ cancelable: false },
		);
	}

	render() {

		if (this.state.isLoading) {
			return (
				<View style={[GlobalStyle.container, { backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor, justifyContent: 'center', alignItems: 'center' }]}>
					<ActivityIndicator color={multiTheme[this.props.chosenTheme].subTXTHeadingColor} size='large' />
				</View>
			)
		}

		return (
			<View style={[GlobalStyle.container, { backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor }]}>
				<View style={{
					width: '100%',
					padding: SafeArea,
					backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor,
					shadowColor: "#000",
					shadowOffset: {
						width: 0,
						height: 1,
					},
					shadowOpacity: 0.22,
					shadowRadius: 2.22,

					elevation: 3
				}}>
					<View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
						<Text style={{ fontFamily: NeoMedium, fontSize: 18, color: multiTheme[this.props.chosenTheme].fontColor }}>{this.state.user_fullname}</Text>
						<Text style={{ fontFamily: NeoMedium, fontSize: 10, color: WhiteColor, paddingVertical: 5, paddingHorizontal: 10, backgroundColor: GreenColor, borderRadius: 10, marginTop: 3 }}>Mobile Operator</Text>
					</View>
					<Text style={{ fontFamily: Neo, fontSize: 14, marginBottom: 5, color: multiTheme[this.props.chosenTheme].subTXTHeadingColor }}>{this.state.user_email}</Text>
				</View>
				<TouchableOpacity style={{ height: 60, paddingHorizontal: SafeArea, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: LightGrayColor }}
					onPress={this.handleLogout}>
					<FontAwesome
						style={{ width: 50 }}
						name='sign-out'
						size={28}
						color={GrayColor}
					/>
					<View style={{ flexDirection: 'row', height: '100%', justifyContent: 'flex-start', alignItems: 'center' }}>
						<Text numberOfLines={1} style={{ fontFamily: Neo, fontSize: 16, width: '100%', color: multiTheme[this.props.chosenTheme].fontColor }}>{multiLanguageText[this.props.chosenLanguage].logout}</Text>
					</View>
				</TouchableOpacity>
				<CustomToast ref={(ref) => { this.toast = ref; }} />
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		chosenLanguage: state.counterOperation.language,
		chosenTheme: state.counterOperation.theme
	};
};


export default connect(mapStateToProps)(SettingScreen);
