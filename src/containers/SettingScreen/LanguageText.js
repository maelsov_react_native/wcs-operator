const multiLanguageText = {
	EN: {
		loginModal: 'You need to login',
		buttonLogin: 'Login',
		profile: 'Profile',
		jobvacancy: 'Job Vacancy List',
		changeLang: 'Change Language',
		logout: 'Log out',
		engLang: 'English Language',
		indoLang: 'Indonesia Language',
		saveLang: 'Save',
		profileInfo: 'Account Information',
		detailEvent: 'Event Detail',
		bookmark: 'Bookmark List',
		profileEdit: 'Profile Setting',
		changePass: 'Change Password',
		// header meru
		subJudul1: 'Personal Setting',
		subJudul2: 'Content',
		subJudul3: 'More',
		//  ddddddddddd
		articleHeader: 'Article List',
		newsHeader: 'News List',
		eventHeader: 'Event List',
		cancel: 'Cancel',
		loginUser: 'Login',
		registerUser: 'Register',
		// Theme Change
		changeTheme: 'Change Theme',
		darkModeTitle: 'Dark Mode',
		lightModeTitle: 'Light Mode',
		// Problem
		problem: 'There is a problem when retrieve data',
		btnRetry: 'Retry'
	},
	ID: {
		loginModal: 'Harap login untuk mengakses fitur ini',
		buttonLogin: 'Masuk',
		profile: 'Profil',
		jobvacancy: 'Penawaran Kerja',
		changeLang: 'Pilih Bahasa',
		logout: 'Keluar',
		engLang: 'Bahasa Inggris',
		indoLang: 'Bahasa Indonesia',
		saveLang: 'Simpan',
		profileInfo: 'Informasi Akun',
		detailEvent: 'Detail Event',
		bookmark: 'List Tertanda',
		profileEdit: 'Pengaturan Profil',
		changePass: 'Ubah Kata Sandi',
		// header meru
		subJudul1: 'Pengaturan Pribadi',
		subJudul2: 'Konten',
		subJudul3: 'Lainnya',
		// more header
		articleHeader: 'Daftar Artikel',
		newsHeader: 'Daftar Berita',
		eventHeader: 'Daftar Acara',
		cancel: 'Batal',
		loginUser: 'Masuk',
		registerUser: 'Daftar',
		// Theme Change
		changeTheme: 'Ganti Tema',
		darkModeTitle: 'Mode Gelap',
		lightModeTitle: 'Mode Terang',
		// Problem
		problem: 'Ada masalah saat pengambilan data',
		btnRetry: 'Coba lagi'

	}
}

export default multiLanguageText