import {
    StyleSheet,
    Platform,
    Dimensions
} from 'react-native';
import { Neo, NeoLight, NeoMedium } from '../../GlobalConfig'

const { width, height } = Dimensions.get('window')


const GlobalStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff'
    },
    imageContent: {
        // elevation: 8,
        // borderRadius: 100
    },
    touchableAvatar: {
        // borderRadius: 100
    },
    avatar: {
        width: 150,
        height: 150,
        // borderRadius: 10,
        backgroundColor: '#ecf0f1',
    },
    userInfo: {
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 15
    },
    userName: {
        fontFamily: NeoMedium,
        fontSize: 18,
        color: '#000'
    },
    userEmail: {
        fontFamily: 'Roboto',
        fontSize: 18,
        color: '#000'
    },
    buttonContent: {
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#cf212a',
        backgroundColor: '#cf212a',
        marginTop: 50
    },
    buttonStyle: {
        justifyContent: 'center',
        marginVertical: 5,
        marginHorizontal: 20,
    },
    textButtonStyle: {
        fontFamily: Neo,
        fontSize: 16,
        color: '#ffffff',
        textAlign: 'center'
    }
});

export default GlobalStyle;