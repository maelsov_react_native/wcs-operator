import React, { Component, PureComponent } from 'react';
import { Platform, Image, Text, View, Dimensions, ScrollView, FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Fontisto from 'react-native-vector-icons/Fontisto'
import GlobalStyle from '../../GlobalStyle'
import multiLanguageText from '../ScanQRScreen/LanguageText'
import multiTheme from '../../multiTheme'
import { GrayColor, NeoMedium, WhiteColor, Neo, SafeArea, LightGrayColor } from '../../GlobalConfig'
import { connect } from 'react-redux';
import { wait } from '../../GlobalFunction';

const profileImg = "http://www.dogster.com/wp-content/uploads/2017/08/A-pomeranian-on-grass.jpg"

const { width, height } = Dimensions.get('window')

class UserItem extends PureComponent {
    render() {
        const item = this.props.item
        const index = this.props.index
        var scanType = ''
        switch (item.type) {
            case 1:
                scanType = 'attendance'
                break;
            case 2:
                scanType = 'merchandise'
                break;
            default:
                break;
        }
        return (
            <View style={{ height: 110, width: 80, alignItems: 'center', marginLeft: index > 0 ? SafeArea : 0 }}>
                <View style={{ height: 50, width: 50, borderRadius: 25, borderWidth: 1, borderColor: LightGrayColor, marginBottom: 10, overflow: 'hidden' }}>
                    <Image style={{ flex: 1 }} source={{ uri: item.image }} />
                </View>
                <View style={{ flex: 1, justifyContent: 'space-between' }}>
                    <Text numberOfLines={2} style={{ fontFamily: Neo, fontSize: 12, textAlign: 'center' }}>{`${item.name}${this.props.index + 1}`}</Text>
                    <Text style={{ fontFamily: NeoMedium, fontSize: 10, textAlign: 'center' }}>{scanType}</Text>
                </View>
            </View>
        )
    }
}

class ScanMenuScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user_email: 'benny.wiguna@mail.com',
            user_avatar: profileImg,
            user_phone: '085248728920',
            user_real_name: 'Benny Susanto Wiguna',
            eventItem: this.props.eventItem ? this.props.eventItem : null
        }
    }

    handleNavigateButton(type) {
        switch (type) {
            case 'attendance':
                Actions.scanQR({
                    title: 'Scan attendance',
                    scanType: 'attendance',
                    eventItem: this.props.eventItem
                })
                break;
            case 'list':
                Actions.scanList({
                    eventItem: this.props.eventItem
                })
                break;
            case 'merchandise':
                Actions.scanQR({
                    title: 'Scan merchandise',
                    scanType: 'merchandise',
                    eventItem: this.props.eventItem
                })
                break;
            default:
                break;
        }
    }

    componentDidMount = () => {
        console.log(this.props.eventItem)
    }

    successScanProduct = ({ data }) => {
        this.setState({
            result: data,
            nowScreen: 'showResult'
        })
    }

    render() {
        return (
            <View style={[GlobalStyle.container, { backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor }]}>
                <ScrollView>
                    <View style={{ width: '100%', paddingHorizontal: 20, marginTop: 20 }}>
                        <View style={{
                            width: '100%',
                            aspectRatio: 1.85,
                            backgroundColor: WhiteColor,
                            overflow: 'hidden',
                            borderRadius: 10,
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 1,
                            },
                            shadowOpacity: 0.20,
                            shadowRadius: 1.41,

                            elevation: 2
                        }}>
                            <Image style={{ flex: 1 }} resizeMode='cover' source={{ uri: this.state.eventItem.event_img_primary }} />
                        </View>
                        <Text style={[GlobalStyle.cardTitleText, { color: multiTheme[this.props.chosenTheme].fontColor }]} numberOfLines={3}>{this.state.eventItem.event_title}</Text>
                    </View>
                    <View style={{ width: '100%', paddingHorizontal: 10, paddingVertical: 10, marginVertical: 10 }}>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity onPress={() => this.handleNavigateButton('list')} style={styles.buttonBox}>
                                <View style={styles.buttonIconBox}>
                                    <Fontisto
                                        name='nav-icon-list-a'
                                        size={20}
                                    />
                                </View>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={styles.buttonText}>Participant list</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity onPress={() => this.handleNavigateButton('attendance')} style={styles.buttonBox}>
                                <View style={styles.buttonIconBox}>
                                    <View style={{ paddingTop: 5, paddingRight: 5 }}>
                                        <View style={{ position: 'absolute', top: -5, right: -5 }}>
                                            <Ionicons
                                                name='md-qr-scanner'
                                                size={24}
                                            />
                                        </View>
                                        <FontAwesome5
                                            name='user-alt'
                                            size={24}
                                        />
                                    </View>
                                </View>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={styles.buttonText}>Scan attendance</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity onPress={() => this.handleNavigateButton('merchandise')} style={styles.buttonBox}>
                                <View style={styles.buttonIconBox}>
                                    <View style={{ paddingTop: 5, paddingRight: 5 }}>
                                        <View style={{ position: 'absolute', top: -5, right: -5 }}>
                                            <Ionicons
                                                name='md-qr-scanner'
                                                size={24}
                                            />
                                        </View>
                                        <FontAwesome5
                                            name='gift'
                                            size={24}
                                        />
                                    </View>
                                </View>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={styles.buttonText}>Scan merchandise</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    {/* <View style={{
                        paddingVertical: 5,
                        backgroundColor: WhiteColor,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.20,
                        shadowRadius: 1.41,

                        elevation: 2
                    }}>
                        <View style={{ width: '100%', paddingLeft: 20, paddingRight: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={{ color: multiTheme[this.props.chosenTheme].fontColor, fontFamily: Neo, fontSize: 16 }}>Recently scanned</Text>
                            <TouchableOpacity onPress={this.refreshRecentScan} style={{ paddingVertical: 5, paddingHorizontal: 10 }}>
                                <MaterialIcons name='refresh' color={multiTheme[this.props.chosenTheme].fontColor} size={24} />
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            contentContainerStyle={{ paddingLeft: 20, paddingRight: 20, marginVertical: 10 }}
                            showsHorizontalScrollIndicator={false}
                            data={this.state.recent_scan}
                            extraData={this.state.recent_scan}
                            keyExtractor={item => item.id}
                            horizontal
                            renderItem={({ item, index }) => <UserItem item={item} index={index} />}
                        />
                    </View> */}
                </ScrollView>
            </View >
        );

    }
}
const styles = StyleSheet.create({
    buttonContainer: {
        height: 80,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },
    buttonBox: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 10,
        backgroundColor: WhiteColor,
        justifyContent: 'space-around',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2
    },
    buttonIconBox: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        fontFamily: NeoMedium,
        fontSize: 14,
        textAlign: 'center'
    }
})

const mapStateToProps = state => {
    return {
        chosenLanguage: state.counterOperation.language,
        chosenTheme: state.counterOperation.theme
    };
};

export default connect(mapStateToProps)(ScanMenuScreen);


