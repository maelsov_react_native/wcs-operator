import React, { Component, PureComponent } from 'react';
import { Platform, Image, Text, View, StyleSheet, FlatList, TouchableOpacity, RefreshControl, TextInput, Animated, Easing } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import GlobalStyle from '../../GlobalStyle'
import { NeoMedium, WhiteColor, Neo, SafeArea, LightGrayColor, BlackColor, GrayColor, GreenColor, RedColor } from '../../GlobalConfig'
import { connect } from 'react-redux';
import { call } from '../../GlobalFunction';
import multiTheme from '../../multiTheme';
import { modelEvent } from '../../models/Event';
import CustomToast from '../../components/CustomToast';
import AsyncStorage from '@react-native-community/async-storage'
import Modal from 'react-native-modal';

class UserItem extends PureComponent {
    render() {
        const item = this.props.item
        const index = this.props.index
        return (
            <View style={{ height: 100, width: '100%', flexDirection: 'row', paddingHorizontal: SafeArea, paddingVertical: 10, alignItems: 'center' }}>
                <TouchableOpacity onPress={() => call(item.phone_number)} disabled={item.phone_number ? false : true} style={{ height: 60, aspectRatio: 1, marginRight: SafeArea }}>
                    <View style={{ flex: 1, borderRadius: 40, borderWidth: 1, borderColor: LightGrayColor, overflow: 'hidden' }}>
                        <Image style={{ flex: 1 }} source={{ uri: item.profile_picture }} />
                    </View>
                    {item.phone_number ? (
                        <View style={{ height: 20, width: 20, borderRadius: 10, backgroundColor: BlackColor, justifyContent: 'center', alignItems: 'center', position: 'absolute', left: 0, bottom: 0 }}>
                            <MaterialIcons name='call' size={16} color={WhiteColor} />
                        </View>
                    ) : null}
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.openManualModal(item.user_id, item.fullname, item.email, item.isAttend, item.isMerchandise)} style={{ flex: 1, alignItems: 'center', flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                        <Text numberOfLines={2} style={{ fontFamily: NeoMedium, fontSize: 12 }}>{item.fullname}</Text>
                        {item.email ? <Text numberOfLines={1} style={{ fontFamily: Neo, fontSize: 10, marginTop: 5 }}>{item.email}</Text> : null}
                        {item.company_name ? <Text numberOfLines={2} style={{ fontFamily: Neo, fontSize: 12, marginTop: 10 }}>{item.company_name}</Text> : null}
                    </View>
                    <View style={{ width: 50, alignItems: 'flex-end' }}>
                        {item.isConfirmed ? (
                            <View style={{ height: 20, width: 30, borderRadius: 15, backgroundColor: BlackColor, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: WhiteColor, fontSize: 12, fontFamily: NeoMedium }}>C</Text>
                            </View>
                        ) : null}
                        {item.isAttend ? (
                            <View style={{ height: 20, width: 30, borderRadius: 15, backgroundColor: BlackColor, justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                                <Text style={{ color: WhiteColor, fontSize: 12, fontFamily: NeoMedium }}>A</Text>
                            </View>
                        ) : null}
                        {item.isMerchandise ? (
                            <View style={{ height: 20, width: 30, borderRadius: 15, backgroundColor: BlackColor, justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                                <Text style={{ color: WhiteColor, fontSize: 12, fontFamily: NeoMedium }}>M</Text>
                            </View>
                        ) : null}
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

class ScanListScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            scanList: [],
            tempScanList: [],
            confirmAmount: 0,
            attendAmount: 0,
            merchandiseAmount: 0,
            search: '',
            chosenUserID: '',
            chosenName: '',
            chosenEmail: '',
            chosenIsAttend: false,
            chosenIsMerchandise: false,
            event_id: this.props.eventItem.event_id,
            isLoading: false,
            helpOpened: false,
            isModalVisible: false
        }
        this.animatedValue = new Animated.Value(0)
    }

    onRefresh = () => {
        let confirmAmount = 0
        let attendAmount = 0
        let merchandiseAmount = 0
        this.setState({ isLoading: true })
        modelEvent.getEventAttendanceList(this.state.event_id, res => {
            console.log(res)
            if (res.status == 200 && res.result.data.attendance_data) {
                res.result.data.attendance_data.forEach(e => {
                    if (e.isConfirmed) confirmAmount++
                    if (e.isAttend) attendAmount++
                    if (e.isMerchandise) merchandiseAmount++
                })
                this.setState({
                    scanList: res.result.data.attendance_data,
                    tempScanList: res.result.data.attendance_data,
                    confirmAmount: confirmAmount,
                    attendAmount: attendAmount,
                    merchandiseAmount: merchandiseAmount
                })
            }
            else {
                this.toast.ShowToastFunction('warning', 'Participant list is empty')
            }
            this.setState({ isLoading: false })
        })
            .catch(err => {
                this.setState({ isLoading: false })
                this.toast.ShowToastFunction('warning', 'Failed to get participant list, please try again later')
            })
    }

    onTextChange = value => {
        let filteredList = []
        this.setState({ search: value },
            () => {
                if (this.state.search) {
                    this.state.scanList.forEach(item => {
                        if (String(item.fullname).toUpperCase().includes(String(value).toUpperCase())) {
                            filteredList = [...filteredList, item]
                        }
                    })
                    this.setState({ tempScanList: filteredList })
                }
                else {
                    this.setState({
                        tempScanList: this.state.scanList
                    })
                }
            })
    }

    openHelp = () => {
        Animated.timing(
            this.animatedValue,
            {
                toValue: 1,
                duration: 400
            },
            { useNativeDriver: true }
        ).start()
        this.setState({
            helpOpened: true
        })
    }

    closeHelp = () => {
        Animated.timing(
            this.animatedValue,
            {
                toValue: 0,
                duration: 400
            },
            { useNativeDriver: true }
        ).start()
        this.setState({
            helpOpened: false
        })
    }

    clearText = () => {
        this.setState({
            search: '',
            tempScanList: this.state.scanList
        })
    }

    openManualModal = (user_id, fullname, email, isAttend, isMerchandise) => {
        this.setState({
            chosenUserID: user_id,
            chosenName: fullname,
            chosenEmail: email,
            chosenIsAttend: isAttend,
            chosenIsMerchandise: isMerchandise,
            isModalVisible: true
        })
    }

    addToList = (scanType, status) => {
        this.setState({ isLoading: true })
        const formdata = new FormData
        formdata.append('event_id', this.props.eventItem.event_id)
        formdata.append('user_id', this.state.chosenUserID)
        formdata.append('mobile_operator_id', this.state.user_id)
        formdata.append('change_type', scanType == 'attendance' ? '1' : '2')
        formdata.append('status_type', status)
        console.log(formdata)
        modelEvent.changeStatusManual(formdata, res => {
            console.log(res)
            if (res.status == 200) {
                if (status == '0') this.toast.ShowToastFunction('success', `Success undo status from ${scanType}`)
                else this.toast.ShowToastFunction('success', `Success update status to ${scanType}`)
                this.onRefresh()
            }
            else if (res.status == 400) {
                switch (res.result.code) {
                    case "scan_qr_err_1":
                        this.toast.ShowToastFunction('warning', 'Failed to update status,please try again later')
                        break;
                    case "scan_qr_err_2":
                        this.toast.ShowToastFunction('warning', 'User is not registered to event')
                        break;
                    default:
                        break;
                }
            }
            else {
                this.toast.ShowToastFunction('warning', 'Failed to update status,please try again later')
            }
            this.hideModal()
            this.setState({ isLoading: false })
        })
            .catch(err => {
                console.log(err)
                this.toast.ShowToastFunction('warning', 'Failed to update status,please try again later')
                this.setState({ isLoading: false })
            })
    }


    hideModal = () => {
        this.setState({
            chosenUserID: '',
            chosenName: '',
            chosenEmail: '',
            chosenIsAttend: false,
            chosenIsMerchandise: false,
            isModalVisible: false
        })
    }

    componentDidMount = () => {
        this.onRefresh()
        AsyncStorage.getItem('user')
            .then(data => {
                if (data) {
                    const parsedData = JSON.parse(data)
                    this.setState({
                        user_id: parsedData.user_data.user_id,
                    })
                }
            })
    }

    render() {
        const modalContent = (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <View style={{
                    width: '100%',
                    aspectRatio: 1,
                    paddingVertical: 20,
                    alignItems: 'center',
                    justifyContent: 'space-around',
                    backgroundColor: WhiteColor,
                    borderRadius: 20,
                    overflow: 'hidden',
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,

                    elevation: 5
                }}>
                    <View style={{ position: 'absolute', top: 10, right: 10 }}>
                        <TouchableOpacity
                            onPress={this.hideModal}
                            style={{ height: 50, aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <AntDesign name='close' size={32} color={BlackColor} />
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: Neo, fontSize: 14 }}>Tap button to change status</Text>
                    <Text style={{ fontFamily: NeoMedium, fontSize: 16 }}>{this.state.chosenName}</Text>
                    <Text style={{ fontFamily: Neo, fontSize: 12 }}>{this.state.chosenEmail ? this.state.chosenEmail : 'No email'}</Text>
                    <View style={{ width: 280, flexDirection: 'row', height: 125, padding: 10, justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ height: '100%', width: 100, alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => this.addToList('attendance', this.state.chosenIsAttend ? 0 : 1)}
                                style={styles.buttonBox}>
                                <FontAwesome5
                                    name={this.state.chosenIsAttend ? 'undo-alt' : 'user-alt'}
                                    size={24}
                                />
                                <View>
                                    <Text style={{ fontFamily: NeoMedium, color: this.state.chosenIsAttend ? RedColor : GreenColor, fontSize: 12, textAlign: 'center' }}>{this.state.chosenIsAttend ? 'Undo' : 'Done'}</Text>
                                    <Text style={styles.buttonText}>attendance</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: '100%', width: 100, alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => this.addToList('merchandise', this.state.chosenIsMerchandise ? 0 : 1)}
                                style={styles.buttonBox}>
                                <FontAwesome5
                                    name={this.state.chosenIsMerchandise ? 'undo-alt' : 'gift'}
                                    size={24}
                                />
                                <View>
                                    <Text style={{ fontFamily: NeoMedium, color: this.state.chosenIsMerchandise ? RedColor : GreenColor, fontSize: 12, textAlign: 'center' }}>{this.state.chosenIsMerchandise ? 'Undo' : 'Done'}</Text>
                                    <Text style={styles.buttonText}>merchandise</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
        const toggleHelp = this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 120],
            easing: Easing.linear
        }, { useNativeDriver: true })
        return (
            <View style={[GlobalStyle.container, { backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor }]}>
                <View style={{
                    width: '100%',
                    paddingLeft: 20,
                    backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.22,
                    shadowRadius: 2.22,

                    elevation: 3,
                    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20
                }}>
                    <View style={{ height: 40, width: '100%', marginBottom: 8, alignItems: 'center', flexDirection: 'row', backgroundColor: WhiteColor, zIndex: 1 }}>
                        <View style={{ flex: 1, borderRadius: 20, borderWidth: 1, paddingLeft: SafeArea, flexDirection: 'row' }}>
                            <TextInput
                                autoFocus
                                style={{ flex: 1 }}
                                value={this.state.search}
                                placeholder='Type here to find participant name'
                                onChangeText={this.onTextChange}
                            />
                            {this.state.search ? (
                                <TouchableOpacity
                                    onPress={this.clearText}
                                    style={{ height: '100%', aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <AntDesign name='close' size={24} color={BlackColor} />
                                </TouchableOpacity>
                            ) : null}
                        </View>
                        <TouchableOpacity onPress={this.state.helpOpened ? this.closeHelp : this.openHelp} style={{ height: '100%', marginHorizontal: SafeArea, alignItems: 'center', paddingTop: 3 }}>
                            {this.state.helpOpened ? (
                                <AntDesign name='closecircleo' size={32} color={BlackColor} />
                            ) : (
                                    <FontAwesome5 name='info-circle' size={32} color={BlackColor} />
                                )}
                        </TouchableOpacity>
                    </View>
                    <Animated.View style={{ height: toggleHelp, opacity: this.animatedValue, width: '100%', paddingRight: 20, marginBottom: 8, justifyContent: 'space-around' }}>
                        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ height: 20, width: 30, borderRadius: 15, backgroundColor: BlackColor, justifyContent: 'center', alignItems: 'center', marginRight: 10 }}>
                                    <Text style={{ color: WhiteColor, fontSize: 12, fontFamily: NeoMedium }}>C</Text>
                                </View>
                                <Text style={{ fontSize: 12, fontFamily: NeoMedium }}>Confirm registration</Text>
                            </View>
                            <Text style={{ fontSize: 12, fontFamily: NeoMedium }}>{this.state.confirmAmount}/{this.state.scanList.length}</Text>
                        </View>
                        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ height: 20, width: 30, borderRadius: 15, backgroundColor: BlackColor, justifyContent: 'center', alignItems: 'center', marginRight: 10 }}>
                                    <Text style={{ color: WhiteColor, fontSize: 12, fontFamily: NeoMedium }}>A</Text>
                                </View>
                                <Text style={{ fontSize: 12, fontFamily: NeoMedium }}>Attendance</Text>
                            </View>
                            <Text style={{ fontSize: 12, fontFamily: NeoMedium }}>{this.state.attendAmount}/{this.state.scanList.length}</Text>
                        </View>
                        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ height: 20, width: 30, borderRadius: 15, backgroundColor: BlackColor, justifyContent: 'center', alignItems: 'center', marginRight: 10 }}>
                                    <Text style={{ color: WhiteColor, fontSize: 12, fontFamily: NeoMedium }}>M</Text>
                                </View>
                                <Text style={{ fontSize: 12, fontFamily: NeoMedium }}>Merchandise</Text>
                            </View>
                            <Text style={{ fontSize: 12, fontFamily: NeoMedium }}>{this.state.merchandiseAmount}/{this.state.scanList.length}</Text>
                        </View>
                    </Animated.View>
                </View>
                <FlatList
                    style={{ marginTop: -SafeArea }}
                    refreshControl={
                        <RefreshControl
                            colors={['#000']}
                            refreshing={this.state.isLoading}
                            onRefresh={this.onRefresh}
                        />
                    }
                    contentContainerStyle={{ paddingTop: SafeArea }}
                    data={this.state.tempScanList}
                    extraData={this.state.tempScanList}
                    keyExtractor={item => item.user_id}
                    ListEmptyComponent={(
                        <View style={{ width: '100%', marginTop: 100, alignItems: 'center' }}>
                            <Text style={{ fontFamily: NeoMedium, fontSize: 16, color: GrayColor }}>No registered participant</Text>
                        </View>
                    )}
                    ItemSeparatorComponent={() => (<View style={{ width: '100%', height: 1, backgroundColor: GrayColor }} />)}
                    renderItem={({ item, index }) => <UserItem item={item} index={index} openManualModal={this.openManualModal} />}
                />
                <CustomToast ref={(ref) => { this.toast = ref; }} />
                <Modal
                    children={modalContent}
                    isVisible={this.state.isModalVisible}
                    onBackButtonPress={this.hideModal}
                    backdropOpacity={0.7} />
            </View>
        );

    }
}

const styles = StyleSheet.create({
    buttonBox: {
        height: 100,
        aspectRatio: 1,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        borderRadius: 10,
        backgroundColor: WhiteColor,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2
    },
    buttonIconBox: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        fontFamily: NeoMedium,
        fontSize: 12,
        textAlign: 'center'
    }
})

const mapStateToProps = state => {
    return {
        chosenLanguage: state.counterOperation.language,
        chosenTheme: state.counterOperation.theme
    };
};

export default connect(mapStateToProps)(ScanListScreen);


