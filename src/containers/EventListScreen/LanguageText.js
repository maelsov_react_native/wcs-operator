const multiLanguageText = {
	EN: {
		tag: 'Tag',
		noConnection: 'Failed to get event list\ncheck your connection',
		refresh: 'Refresh',
		toastSuccess: 'Bookmark successfully added!',
		toastError: 'Bookmark failed to add!',
		loginModal: 'Login to access this feature?',
		buttonLogin : 'Login',
	},
	ID: {
		tag: 'Kategori',
		noConnection: 'Gagal mendapatkan daftar acara\nperiksa koneksi Anda',
		refresh: 'Coba lagi',
		toastSuccess: 'Bookmarks berhasil ditambahkan!',
		toastError: 'Bookmark gagal ditambahkan!',
		loginModal: 'Login untuk mengakses fitur ini?',
		buttonLogin : 'Masuk',
	}
}

export default multiLanguageText