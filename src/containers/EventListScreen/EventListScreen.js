import React, { Component, PureComponent } from 'react';
import {
	Text,
	View,
	ImageBackground,
	TouchableWithoutFeedback,
	TouchableOpacity,
	RefreshControl,
	FlatList,
	Image,
	ActivityIndicator
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import GlobalStyle from '../../GlobalStyle'
import { wait, convertDateTime } from '../../GlobalFunction';
import { modelEvent } from '../../models/Event';
import { connect } from 'react-redux';
import multiTheme from '../../multiTheme'
import { LightGrayColor, BlackColor, GrayColor, NeoMedium, WhiteColor, Images } from '../../GlobalConfig';
import multiLanguageText from './LanguageText';

class NewsItem extends PureComponent {
	render() {
		const item = this.props.item
		return (
			<TouchableWithoutFeedback disabled={this.props.disableClick} onPress={() => this.props.moveToDetail(item.event_id)}>
				<View style={GlobalStyle.newsCard}>
					<View style={GlobalStyle.imageBackgroundStyle}>
						<ImageBackground style={{ flex: 1 }} source={{ uri: item.event_img_primary }} />
					</View>
					<Text style={[GlobalStyle.cardTitleText, { color: multiTheme[this.props.theme].fontColor }]} numberOfLines={3}>{item.event_title}</Text>
					<Text style={GlobalStyle.cardSubtitleText} numberOfLines={1}>{item.event_created_by}, {convertDateTime(item.event_published_timestamp)}</Text>
				</View>
			</TouchableWithoutFeedback>
		)
	}
}

class EventListScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			disableClick: false,
			data: [],
			tempData: [],
			limit: 10,
			offset: 0,
			done: false,
			connectionError: false
		}
	}

	componentDidMount() {
		this.fetchEvent()
	}

	fetchEvent = () => {
		this.setState({ isLoading: true })
		modelEvent.getEventList(this.state.limit, this.state.offset, res => {
			console.warn(this.state.offset)
			if (res.status == 200) {
				if (this.state.offset == 0) {
					console.log(res.result.data.data)
					this.setState({
						data: res.result.data.data,
						tempData: res.result.data.data,
						offset: 10
					})
				}
				else if (res.result.data.data.length > 0 && (res.result.data.data[0].event_id != this.state.tempData[0].event_id)) {
					console.log(res.result.data.data)
					this.setState({
						data: [...this.state.data, ...res.result.data.data],
						tempData: res.result.data.data,
						offset: this.state.offset + 10
					})
				}
				else {
					this.setState({ done: true })
				}
			}
			this.setState({ isLoading: false })
		})
			.catch(err => {
				this.setState({
					isLoading: false,
					connectionError: true
				})
				console.log(err)
			})
	}

	onRefresh = () => {
		this.setState({
			data: [],
			tempData: [],
			offset: 0,
			done: false,
			connectionError: false
		}, this.fetchEvent)
	}

	moveToDetail = (eventID) => {
		this.setState({
			isLoading: true
		}, () => wait(100).then(() => this.setState({ isLoading: false })))

		Actions.detailEvent({
			eventID: eventID
		})
	}

	render() {
		if (this.state.connectionError) {
			return (
				<View style={[GlobalStyle.container, { justifyContent: 'center', alignItems: 'center', backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor }]}>
					<View style={{ height: 100, width: 100 }}>
						<Image
							resizeMode='contain'
							source={Images.NO_CONNECTION}
							style={{ flex: 1, tintColor: BlackColor }} />
					</View>
					<View style={{ width: '80%', marginTop: 20, marginBottom: 50 }}>
						<Text style={[{ fontFamily: NeoMedium, fontSize: 15, textAlign: 'center', color: multiTheme[this.props.chosenTheme].fontColor, lineHeight: 24 }]}>{multiLanguageText[this.props.chosenLanguage].noConnection}</Text>
					</View>
					<View style={{ width: 200, height: 50, padding: 2 }}>
						<TouchableOpacity
							onPress={this.onRefresh}
							disabled={this.state.isLoading}
							style={[{
								flex: 1,
								borderRadius: 30,
								justifyContent: 'center',
								alignItems: 'center'
							},
							!this.state.isLoading ?
								{
									backgroundColor: BlackColor,
									shadowColor: "#000",
									shadowOffset: {
										width: 0,
										height: 1,
									},
									shadowOpacity: 0.22,
									shadowRadius: 2.22,

									elevation: 3,
								} :
								{
									backgroundColor: GrayColor
								}]}>
							{this.state.isLoading ? (
								<ActivityIndicator size={24} color={WhiteColor} />
							) : (
									<Text style={{ fontFamily: NeoMedium, fontSize: 14, color: WhiteColor }}>{multiLanguageText[this.props.chosenLanguage].refresh}</Text>
								)}
						</TouchableOpacity>
					</View>
				</View>
			);
		}

		return (
			<View style={[GlobalStyle.container, { backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor }]} >
				<FlatList
					refreshControl={
						<RefreshControl
							colors={['#000']}
							refreshing={this.state.isLoading}
							onRefresh={this.onRefresh}
						/>
					}
					contentContainerStyle={{ paddingVertical: 20 }}
					scrollEventThrottle={16}
					showsVerticalScrollIndicator={false}
					keyExtractor={item => item.event_id}
					extraData={this.state.data}
					data={this.state.data}
					ItemSeparatorComponent={() => (
						<View style={{ height: 2, width: '100%', backgroundColor: LightGrayColor, marginBottom: 20 }} />
					)}
					onEndReached={this.state.data.length > 9 && !this.state.isLoading && !this.state.done ? this.fetchEvent : null}
					onEndReachedThreshold={0.2}
					renderItem={({ item, index }) => (
						<NewsItem
							item={item}
							index={index}
							theme={this.props.chosenTheme}
							disableClick={this.state.disableClick}
							moveToDetail={this.moveToDetail} />
					)}
				/>
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		chosenLanguage: state.counterOperation.language,
		chosenTheme: state.counterOperation.theme
	};
};

export default connect(mapStateToProps)(EventListScreen);