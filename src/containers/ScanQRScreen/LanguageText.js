const multiLanguageText = {
	EN: {
		loginModal: 'You need to login first',
		buttonLogin: 'Login',
		toastSuccess: 'Delete bookmark successfully!',
		toastError: 'Delete bookmark not successfully!',
		modalTitle: 'Are you sure you want to delete bookmark?',
		modalButtonYes: 'Yes',
		modalButtonNo: 'No',
		ActivityIndicator: 'Loading....',
		showQRCode: 'Show QR Code',
		scanAttend: 'Scan Attendance',
		btnBack: 'Back',
		profileInfo: 'Account Information',
		phoneNumber: 'Phone Number',
		email: 'Email',
		companyProfile: 'Company Profile',
		scanDone: 'Cancel',
		qrMerchandise: 'Scan for Merchandise',
	},
	ID: {
		loginModal: 'Harap masuk untuk mengakses fitur ini',
		buttonLogin: 'Masuk',
		modalButtonYes: 'Yakin',
		modalButtonNo: 'Tidak',
		toastSuccess: 'Bookmark berhasil dihapus!',
		toastError: 'Bookmark gagal dihapus!',
		ActivityIndicator: 'Memuat....',
		modalTitle: 'Apakah Anda yakin untuk menghapus bookmark?',
		showQRCode: 'Menunjukan QR Code',
		scanAttend: 'Scan Kehadiran',
		btnBack: 'Kembali',
		profileInfo: 'Informasi Akun',
		phoneNumber: 'Nomor Telp.',
		email: 'Surel',
		companyProfile: 'Profil Perusahaan',
		scanDone: 'Batal',
		qrMerchandise: 'Scan untuk Merchandise',

	}
}

export default multiLanguageText