import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, Dimensions, ActivityIndicator, TouchableOpacity, Animated } from 'react-native';
import { Actions } from 'react-native-router-flux'
import Feather from 'react-native-vector-icons/Feather'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import AsyncStorage from '@react-native-community/async-storage'
import GlobalStyle from '../../GlobalStyle'
import multiLanguageText from './LanguageText'
import styles from './styles'
import Sound from 'react-native-sound'

import { RNCamera } from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';
import { connect } from 'react-redux';
import { wait } from '../../GlobalFunction';
import { BlackColor, WhiteColor, NeoMedium, GrayColor, SuccessColor, LightGrayColor, SafeArea } from '../../GlobalConfig';
import CustomToast from '../../components/CustomToast';
import { wcsmAesDecrypt } from '../../libraries/ecryption';
import { modelUser } from '../../models/User';
import { modelEvent } from '../../models/Event';

const { width, height } = Dimensions.get('window')

class ScanQRScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            scannedUserID: '',
            scannedName: '',
            isLoading: false,
            initialLoad: true,
            flashOn: false,
            soundOn: true,
            disableCamera: false
        }
        this.soundListener = new Sound('scan_alert.mp3')
    }

    componentDidMount() {
        AsyncStorage.getItem('user').then(data => {
            if (data) {
                const parsedData = JSON.parse(data)
                this.setState({
                    user_id: parsedData.user_data.user_id
                })
            }
            else {
                this.toast.ShowToastFunction('error', 'Need relogin')
            }
        })
        wait(100).then(() => {
            this.setState({ initialLoad: false })
        })
    }

    successScanProduct = ({ data }) => {
        // console.log(data)
        if (!this.state.isLoading || !this.state.scannedUserID) {
            this.setState({ isLoading: true }, () => {
                if (String(data).substr(0, 5) == 'wcsid') {
                    wcsmAesDecrypt(String(data).substring(5, data.length))
                        .then((decryptedUserID) => {
                            if (!isNaN(decryptedUserID)) {
                                if (this.state.soundOn) this.soundListener.play();
                                modelUser.getProfile(decryptedUserID, res => {
                                    console.log(res)
                                    if (res.status == 200) {
                                        this.setState({
                                            scannedUserID: decryptedUserID,
                                            scannedName: res.result.data.fullname
                                        })
                                    }
                                    else this.toast.ShowToastFunction('error', 'User not found')
                                    this.setState({ isLoading: false })
                                })
                            }
                            else {
                                this.toast.ShowToastFunction('error', 'Invalid QR code')
                                this.setState({ isLoading: false })
                            }
                        })
                        .catch(err => {
                            console.log(err)
                            this.toast.ShowToastFunction('error', 'Invalid QR code')
                            this.setState({ isLoading: false })
                        })
                }
                else {
                    this.setState({ isLoading: false })
                    this.toast.ShowToastFunction('error', 'Invalid QR code')
                }
            })
        }
    }

    toggleFlash = () => {
        if (this.state.flashOn) this.toast.ShowToastFunction('success', 'Flash off')
        else this.toast.ShowToastFunction('success', 'Flash on')
        this.setState({ flashOn: !this.state.flashOn })
    }

    toggleSound = () => {
        if (this.state.soundOn) this.toast.ShowToastFunction('success', 'Scan sound off')
        else this.toast.ShowToastFunction('success', 'Scan sound on')
        this.setState({ soundOn: !this.state.soundOn })
    }

    addToList = () => {
        this.setState({ isLoading: true })
        const formdata = new FormData
        formdata.append('event_id', this.props.eventItem.event_id)
        formdata.append('user_id', this.state.scannedUserID)
        formdata.append('scan_type', this.props.scanType == 'attendance' ? '1' : '2')
        formdata.append('mobile_operator_id', this.state.user_id)
        console.log(formdata)
        modelEvent.scanAttendance(formdata, res => {
            console.log(res)
            if (res.status == 200) {
                this.toast.ShowToastFunction('success', 'Success add to list')
                this.clearScanned()
            }
            else if (res.status == 400) {
                switch (res.result.code) {
                    case "scan_qr_err_1":
                        this.toast.ShowToastFunction('warning', 'Failed to add user to the list, try again later')
                        this.clearScanned()
                        break;
                    case "scan_qr_err_2":
                        this.toast.ShowToastFunction('warning', 'User is not registered to event')
                        break;
                    default:
                        break;
                }
            }
            else {
                this.toast.ShowToastFunction('error', 'Failed to add user to the list, try again later')
                this.clearScanned()
            }
            this.setState({ isLoading: false })
        })
            .catch(err => {
                console.log(err)
                this.setState({ isLoading: false })
            })
    }

    clearScanned = () => {
        this.setState({
            scannedUserID: '',
            scannedName: ''
        })
    }

    handleOpenList = () => {
        this.setState({
            disableCamera: true
        })
        Actions.scanList({
            eventItem: this.props.eventItem
        })
    }

    disableCamera = () => {
        this.toast.ShowToastFunction('success', 'Camera disabled')
        this.setState({
            disableCamera: true
        })
    }

    enableCamera = () => {
        this.setState({
            disableCamera: false
        })
    }

    render() {
        return (
            <View style={GlobalStyle.container}>
                <View style={{ height: 60, width: '100%', backgroundColor: 'rgba(0,0,0,0.5)', flexDirection: 'row', position: 'absolute', top: 0, zIndex: 2 }}>
                    <TouchableOpacity onPress={() => Actions.pop()} style={{ height: '100%', aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Feather name={Platform.OS == 'android' ? "arrow-left" : "chevron-left"} size={24} color={WhiteColor} />
                    </TouchableOpacity>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ fontFamily: NeoMedium, fontSize: 18, color: WhiteColor }}>{this.props.title}</Text>
                    </View>
                    <TouchableOpacity onPress={this.handleOpenList} style={{ height: '100%', aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <FontAwesome5
                            name='clipboard-check'
                            size={24}
                            color={WhiteColor}
                        />
                    </TouchableOpacity>
                </View>
                {this.state.initialLoad ?
                    (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size={32} color={BlackColor} />
                        </View>
                    ) :
                    (
                        <>
                            {this.state.disableCamera ? (
                                <View style={{ flex: 1, zIndex: 1, backgroundColor: GrayColor, justifyContent: 'center', alignItems: 'center' }}>
                                    <View style={{ width: '60%', alignItems: 'center' }}>
                                        <Text style={{ fontFamily: NeoMedium, color: WhiteColor, textAlign: 'center', fontSize: 16 }}>Camera is disabled to reduce battery usage</Text>
                                        <TouchableOpacity onPress={this.enableCamera} style={{ height: 50, width: 140, borderRadius: 30, marginTop: 40, backgroundColor: BlackColor, justifyContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ fontFamily: NeoMedium, fontSize: 14, color: WhiteColor }}>Enable camera</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            ) : (
                                    <View style={{ flex: 1, zIndex: 1 }}>
                                        <RNCamera
                                            ref={cam => { this.camera = cam }}
                                            style={{ flex: 1 }}
                                            flashMode={this.state.flashOn ? RNCamera.Constants.FlashMode.torch : RNCamera.Constants.FlashMode.off}
                                            captureAudio={false}
                                            onBarCodeRead={this.state.isLoading || this.state.scannedUserID ? null : this.successScanProduct}
                                            barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
                                        >
                                            <BarcodeMask
                                                width={200}
                                                height={200}
                                                edgeColor={'#fff'}
                                                showAnimatedLine
                                                transparency={0.8}
                                                animatedLineColor={"red"}
                                                lineAnimationDuration={3000}
                                                edgeBorderWidth={4} />
                                        </RNCamera>
                                        <View style={{ position: 'absolute', bottom: '5%', alignSelf: 'center', width: 250, flexDirection: 'row', justifyContent: 'space-around' }}>
                                            <TouchableOpacity onPress={this.toggleFlash} style={{ height: 50, width: 50, borderRadius: 25, borderWidth: 1, borderColor: WhiteColor, justifyContent: 'center', alignItems: 'center' }}>
                                                <MaterialCommunityIcons
                                                    name={this.state.flashOn ? 'flash' : 'flash-off'}
                                                    size={20}
                                                    color={WhiteColor}
                                                />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={this.toggleSound} style={{ height: 50, width: 50, borderRadius: 25, borderWidth: 1, borderColor: WhiteColor, justifyContent: 'center', alignItems: 'center' }}>
                                                <MaterialCommunityIcons
                                                    name={this.state.soundOn ? 'bell' : 'bell-off'}
                                                    size={20}
                                                    color={WhiteColor}
                                                />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={this.disableCamera} style={{ height: 50, width: 50, borderRadius: 25, borderWidth: 1, borderColor: WhiteColor, justifyContent: 'center', alignItems: 'center' }}>
                                                <MaterialCommunityIcons
                                                    name={'camera-off'}
                                                    size={20}
                                                    color={WhiteColor}
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                )}
                            <View style={{ height: 200, width: '100%', paddingHorizontal: 20, zIndex: 2, backgroundColor: WhiteColor, justifyContent: 'space-evenly' }}>
                                <View style={{ width: '100%', height: 50, padding: 2, flexDirection: 'row' }}>
                                    <View style={{ height: '100%', marginRight: 20, justifyContent: 'center' }}>
                                        <FontAwesome5
                                            name='user-alt'
                                            size={20}
                                            color={GrayColor}
                                        />
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={[{ fontSize: 14 }, !this.state.scannedName && { color: GrayColor }]}>{this.state.scannedName ? this.state.scannedName : 'Scanned user name will appear here'}</Text>
                                    </View>
                                    {this.state.scannedName != '' && (
                                        <View style={{ height: '100%', marginLeft: 20, justifyContent: 'center' }}>
                                            <FontAwesome5
                                                name='check-circle'
                                                size={18}
                                                color={SuccessColor}
                                            />
                                        </View>
                                    )}
                                </View>
                                <View style={{ width: '100%', height: 50, padding: 2 }}>
                                    <TouchableOpacity
                                        onPress={this.addToList}
                                        disabled={!this.state.scannedUserID || this.state.isLoading}
                                        style={[{
                                            flex: 1,
                                            borderRadius: 30,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        },
                                        this.state.scannedName != '' ?
                                            {
                                                backgroundColor: BlackColor,
                                                shadowColor: "#000",
                                                shadowOffset: {
                                                    width: 0,
                                                    height: 1,
                                                },
                                                shadowOpacity: 0.22,
                                                shadowRadius: 2.22,

                                                elevation: 3,
                                            } :
                                            {
                                                backgroundColor: GrayColor
                                            }]}>
                                        {this.state.isLoading ? (
                                            <ActivityIndicator size={24} color={WhiteColor} />
                                        ) : (
                                                <Text style={{ fontFamily: NeoMedium, fontSize: 14, color: WhiteColor }}>{this.state.scannedUserID ? `Add to ${this.props.scanType} list` : 'Scan to add list'}</Text>
                                            )}
                                    </TouchableOpacity>
                                </View>
                                <View style={{ width: '100%', height: 50, padding: 2 }}>
                                    <TouchableOpacity
                                        onPress={this.clearScanned}
                                        disabled={!this.state.scannedUserID || this.state.isLoading}
                                        style={{
                                            flex: 1,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                        <Text style={{ fontFamily: NeoMedium, fontSize: 14, color: this.state.scannedUserID != '' ? BlackColor : GrayColor }}>Clear</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </>
                    )
                }
                <CustomToast ref={toast => { this.toast = toast }} />
            </View >
        );
    }
}

const mapStateToProps = state => {
    return {
        chosenLanguage: state.counterOperation.language,
        chosenTheme: state.counterOperation.theme
    };
};

export default connect(mapStateToProps)(ScanQRScreen);


