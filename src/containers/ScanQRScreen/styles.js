import {
    StyleSheet,
    Platform,
    Dimensions
} from 'react-native';
import { Neo, NeoLight, NeoMedium } from '../../GlobalConfig'

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
    avatar: {
        width: 120,
        height: 120
    },
    userInfo: {
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 15
    },
    userName: {
        fontFamily: NeoMedium,
        fontSize: 18,
        color: '#000'
    },
    userNameDark: {
        fontFamily: NeoMedium,
        fontSize: 18,
        color: '#fff'
    },
    userInformation: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#000'
    },
    userInformationDark: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#fff'
    },
    button: {
        width: '90%',
        borderRadius: 10,
        backgroundColor: '#787878',
        paddingVertical: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        fontFamily: 'NeoSansStd-Medium',
        color: '#fff'
    },
    leftSide: {
        width: 80,
        height: '100%',
        borderRightWidth: 2,
        borderRightColor: '#fff',
        backgroundColor: '#000'
    },
    leftSideDark: {
        width: 80,
        height: '100%',
        borderRightWidth: 2,
        borderRightColor: '#000',
        backgroundColor: '#fff'
    },
    rightSide: {
        width: width - 80,
        height: '100%',
        backgroundColor: '#fff',
        paddingTop: 50,
        paddingHorizontal: 10
    },
    rightSideDark: {
        width: width - 80,
        height: '100%',
        backgroundColor: '#2d2d2d',
        paddingTop: 50,
        paddingHorizontal: 10
    }
});

export default styles;