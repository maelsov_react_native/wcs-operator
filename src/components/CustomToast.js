import React, { Component } from 'react';
import { StyleSheet, Text, Animated, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { SuccessColor, ErrorColor, WarningColor, NeoMedium, WhiteColor } from '../GlobalConfig';

const { height } = Dimensions.get('screen')
export default class CustomToast extends Component {
    constructor() {
        super();
        this.animatedToastValue = new Animated.Value(0);
        this.state = {
            showToast: false
        }

        this.toastMessage = '';
        this.toastBackgroundColor = '';
    }

    componentWillUnmount() {
        this.timerID && clearTimeout(this.timerID);
    }

    ShowToastFunction(messageType = "success", message = "No Text", duration = 2000) {
        this.toastMessage = message;
        switch (messageType) {
            case 'success':
                this.toastBackgroundColor = SuccessColor;
                break;
            case 'warning':
                this.toastBackgroundColor = WarningColor;
                break;
            case 'error':
                this.toastBackgroundColor = ErrorColor;
                break;
        }

        this.animatedToastValue.setValue(0)
        this.setState({ showToast: true }, () => {
            Animated.timing(
                this.animatedToastValue,
                {
                    toValue: 1,
                    duration: 500,
                    useNativeDriver: true
                }
            ).start(() => this.HideToastFunction(duration))
        });

    }

    HideToastFunction = (duration) => {
        this.timerID = setTimeout(() => {
            Animated.timing(
                this.animatedToastValue,
                {
                    toValue: 0,
                    duration: 300,
                    useNativeDriver: true
                }
            ).start(() => {
                this.setState({
                    showToast: false,
                    toastMessage: ''
                });
                clearTimeout(this.timerID);
            })
        }, duration);
    }

    render() {
        const animatedPosition = this.animatedToastValue.interpolate({
            inputRange: [0, 1],
            outputRange: [-50, 0],
        });
        const animatedOpacity = this.animatedToastValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
        });
        if (this.state.showToast) {
            return (
                <Animated.View style={[styles.animatedToastView, {
                    opacity: animatedOpacity, backgroundColor: this.toastBackgroundColor,
                    transform: [{
                        translateY: animatedPosition
                    }],
                }]}>
                    <Text style={[styles.ToastBoxInsideText, { color: this.props.textColor }]}>{this.toastMessage}</Text>
                </Animated.View>
            );
        }
        else {
            return null;
        }
    }
}


CustomToast.propTypes = {
    backgroundColor: PropTypes.string,
    position: PropTypes.oneOf([
        'top',
        'bottom'
    ]),
    textColor: PropTypes.string,
    toastType: PropTypes.oneOf([
        'success',
        'warning',
        'error'
    ])
};

CustomToast.defaultProps = {
    backgroundColor: '#666666',
    position: 'top',
    textColor: '#fff'
}

const styles = StyleSheet.create({
    animatedToastView: {
        width: '100%',
        paddingVertical: 10,
        zIndex: 9999,
        position: 'absolute',
        alignSelf: 'center',
        justifyContent: 'center'
    },
    ToastBoxInsideText: {
        fontSize: 12,
        fontFamily: NeoMedium,
        color: WhiteColor,
        textAlign: 'center'
    }
});