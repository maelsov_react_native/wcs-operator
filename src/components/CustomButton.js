import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Platform, TouchableNativeFeedback, ViewPropTypes, ActivityIndicator } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome'

import { GrayColor, NeoMedium, WCSGreenColor, WhiteColor, LightBlueColor } from '../GlobalConfig';
import PropTypes from 'prop-types';

export default class CustomButton extends PureComponent {
	static propTypes = {
		containerStyle: ViewPropTypes.style,
		style: ViewPropTypes.style,
		label: PropTypes.string,
		onPress: PropTypes.func,
	}

	render() {
		let leftView, rightView
		const { label, onPress, style, icon, position = 'left', ...props } = this.props;
		const buttonStyle = {
			width: '100%',
			height: 40,
			marginBottom: 20,
			borderRadius: 5,
			backgroundColor: WCSGreenColor,
			flexDirection: 'row',
			alignItems: 'center'
		}
		const buttonTextStyle = {
			flex: 1,
			justifyContent: 'center',
			alignItems: 'center',
			paddingTop: Platform.OS == 'ios' ? 5 : 0
		}
		const buttonShadowStyle = {
			backgroundColor: props.customButtonColor ? props.customButtonColor : WCSGreenColor,
			borderRadius: 5,
			shadowColor: "#000",
			shadowOffset: {
				width: 0,
				height: 1,
			},
			shadowOpacity: 0.22,
			shadowRadius: 2.22,
			elevation: 3,
		}
		const buttonDisabledShadowStyle = {
			backgroundColor: GrayColor,
			borderRadius: 5,
			shadowColor: "#000",
			shadowOffset: {
				width: 0,
				height: 1,
			},
			shadowOpacity: 0.20,
			shadowRadius: 1.41,

			elevation: 1,
		}
		const buttonText = {
			fontSize: 14,
			letterSpacing: 0.3,
			textAlign: 'center',
			fontFamily: NeoMedium,
			color: props.customTextColor ? props.customTextColor : WhiteColor
		}

		const emptyView = (<View style={{ height: 40, aspectRatio: 1 }} />)

		if (icon) {
			if (position == 'left') {
				leftView = (
					<>
						<View style={{ height: 40, aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }}>
							{icon}
						</View>
					</>
				)
				rightView = emptyView
			}
			else {
				leftView = emptyView
				rightView = (
					<>
						<View style={{ height: 40, aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }}>
							{icon}
						</View>
					</>
				)
			}
		}

		if (Platform.OS == 'android') {
			return (
				<TouchableNativeFeedback
					{...props}
					onPress={onPress}
					disabled={props.disabled || props.isLoading}>
					<View style={[buttonStyle,
						!props.isLinkedIn && { justifyContent: 'center' },
						props.disabled || props.isLoading ? buttonDisabledShadowStyle : buttonShadowStyle,
						{ ...style }]}>
						{props.isLinkedIn && (
							<>
								<View style={{ height: 40, aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }}>
									<FontAwesome
										size={24}
										color={WhiteColor}
										name={`linkedin`}
									/>
								</View>
								<View style={{ height: '100%', justifyContent: 'center' }}>
									<View style={{ height: 30, width: 2, backgroundColor: WhiteColor, borderRadius: 40 }} />
								</View>
							</>
						)}
						{leftView}
						<View style={buttonTextStyle}>
							{props.isLoading ?
								<ActivityIndicator color={props.customTextColor ? props.customTextColor : WhiteColor} size='small' /> :
								<Text style={buttonText}>{label}</Text>}
						</View>
						{rightView}
					</View>
				</TouchableNativeFeedback>
			)
		}
		return (
			<TouchableOpacity
				{...props}
				onPress={onPress}
				disabled={props.disabled || props.isLoading}>
				<View style={[buttonStyle,
					!props.isLinkedIn && { justifyContent: 'center' },
					props.disabled || props.isLoading ? buttonDisabledShadowStyle : buttonShadowStyle,
					{ ...style }]}>
					{leftView}
					<View style={buttonTextStyle}>
						{props.isLoading ?
							<ActivityIndicator color={props.customTextColor ? props.customTextColor : WhiteColor} size='small' /> :
							<Text style={buttonText}>{label}</Text>}
					</View>
					{rightView}
				</View>
			</TouchableOpacity>
		)
	}
}