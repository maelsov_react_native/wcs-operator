import React, { PureComponent } from "react";
import { View, TextInput, TouchableOpacity, ViewPropTypes } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Feather from 'react-native-vector-icons/Feather'
import { GrayColor, Neo } from "../GlobalConfig";
import multiTheme from "../multiTheme";
import PropTypes from 'prop-types';

export default class CustomTextInputWithIcon extends PureComponent {
    static propTypes = {
        style: ViewPropTypes.style,
        autoFocus: PropTypes.bool,
        editbale: PropTypes.bool,
        textColor: PropTypes.string,
        onChangeText: PropTypes.func,
        value: PropTypes.string,
        placeholder: PropTypes.string,
        label: PropTypes.string,
        isPassword: PropTypes.bool,
        maxLength: PropTypes.number,
        autoCapitalize: PropTypes.string,
        keyboardType: PropTypes.string
    }

    constructor(props) {
        super(props)
        this.state = {
            focus: false,
            hideInput: props.isPassword ? true : false
        }
    }

	/**
	 * Function called when Input box are focused
	 */
    onFocusInput = () => this.setState({ focus: true })

	/**
	 * Function called when Input box are not focused
	 */
    onEndFocusInput = () => this.setState({ focus: false })

    focus = () => this.textInput && this.textInput.focus()

    render() {
        const { label, chosenTheme = 'Light', inputIcon = 'email', ...props } = this.props;
        const inputBoxStyle = {
            width: '100%',
            borderBottomWidth: 1,
            borderBottomColor: GrayColor,
            height: 40,
            flexDirection: 'row',
            paddingBottom: 10,
            marginBottom: 20
        };
        const iconContainerStyle = {
            height: 30,
            width: 40,
            justifyContent: 'center',
            alignItems: 'center',
        };
        const textInputContainerStyle = {
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            paddingLeft: 10
        };
        const textInputStyle = {
            flex: 1,
            fontSize: 14,
            letterSpacing: 0.2,
            fontFamily: Neo,
            color: multiTheme[chosenTheme].fontColor,
            padding: 0
        };
        return (
            <View style={[inputBoxStyle, this.state.focus && { borderBottomColor: multiTheme[chosenTheme].clickableColor }]}>
                <View style={iconContainerStyle}>
                    {props.isMaterialIcon && (
                        <MaterialCommunityIcons
                            size={24}
                            color={(this.state.focus || props.value) ? multiTheme[chosenTheme].clickableColor : GrayColor}
                            name={inputIcon}
                        />
                    )}
                    {props.isFontAwesomeIcon && (
                        <FontAwesome
                            size={24}
                            color={(this.state.focus || props.value) ? multiTheme[chosenTheme].clickableColor : GrayColor}
                            name={inputIcon}
                        />
                    )}
                </View>
                <View style={[{ height: 30, width: 1, borderRadius: 5, alignSelf: 'center' }, this.state.focus ? { backgroundColor: multiTheme[chosenTheme].clickableColor } : { backgroundColor: GrayColor }]} />
                <View style={textInputContainerStyle}>
                    <TextInput
                        {...props}
                        ref={refs => this.textInput = refs}
                        secureTextEntry={this.state.hideInput}
                        style={textInputStyle}
                        placeholder={!this.props.disabled ? props.placeholder : ''}
                        placeholderTextColor={GrayColor}
                        editable={this.props.disabled ? false : true}
                        onFocus={this.onFocusInput}
                        onEndEditing={this.onEndFocusInput}
                    />
                    {props.isPassword && props.withEye &&
                        <TouchableOpacity style={{ width: 40, alignItems: 'center' }} onPress={() => this.setState({ hideInput: !this.state.hideInput })}>
                            <Feather
                                size={24}
                                color={this.state.focus ? multiTheme[chosenTheme].clickableColor : GrayColor}
                                name={this.state.hideInput ? 'eye-off' : 'eye'}
                            />
                        </TouchableOpacity>}
                </View>
            </View>
        )
    }
}