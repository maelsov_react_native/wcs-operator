import { Platform, Dimensions } from 'react-native';
//Metrics
export const SafeArea = 16

//Fonts
export const DefaultText = Platform.select({
    ios: 'Helvetica',
    android: 'Roboto',
    default: Neo
})
export const NeoLight = 'NeoSansStd-Light'
export const Neo = 'NeoSansStd-Regular'
export const NeoMedium = 'NeoSansStd-Medium'

export const ButtonGrayColor = '#787878'
export const WCSGreenColor = '#197280'
//Colors
export const BlackColor = '#000000'
export const WhiteColor = '#fff'
export const GrayColor = '#9f9f9f'
export const LightGrayColor = '#e3e3e3'
export const LinkedInColor = '#0077B5'
export const RedColor = '#960b0b'
export const GreenColor = '#5BB739'
export const OrangeColor = '#F28733'
export const LightBlueColor = '#007bb6'


export const RatingColor = '#FDCC0D'

export const SuccessColor = GreenColor
export const WarningColor = OrangeColor
export const ErrorColor = RedColor

export const Images = {
    LOGO : require('./images/logo.png'),
    NO_CONNECTION : require('./images/noconn.png'),
    PLACEHOLDER : require('./images/placeholder.jpg')
}

export const Metrics = {
    SAFE_AREA: 16,
    NAVBAR_HEIGHT: 56,
    SCREEN_WIDTH: Dimensions.get('window').width,
    SCREEN_HEIGHT: Dimensions.get('window').height,
}