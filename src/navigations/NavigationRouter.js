import React, { Component, PureComponent } from 'react';
import { View, Text, Easing, TouchableOpacity, Platform, BackHandler } from 'react-native';
import { Router, Scene, Actions, Stack } from 'react-native-router-flux';
import { connect } from "react-redux";
import Feather from 'react-native-vector-icons/Feather'
import FontAwesome from 'react-native-vector-icons/FontAwesome'


import { BlackColor, NeoMedium } from '../GlobalConfig';
import multiTheme from '../multiTheme'
import SplashScreen from '../containers/SplashScreen/SplashScreen'
import EventListScreen from '../containers/EventListScreen/EventListScreen'
import ScanMenuScreen from '../containers/ScanMenuScreen/ScanMenuScreen';
import ScanQRScreen from '../containers/ScanQRScreen/ScanQRScreen';
import ScanListScreen from '../containers/ScanListScreen/ScanListScreen';
import DetailEventScreen from '../containers/DetailEventScreen/DetailEventScreen';
import SettingScreen from '../containers/SettingScreen/SettingScreen';
import LoginScreen from '../containers/LoginScreen/LoginScreen';
import ImageScreen from '../containers/UtilityScreen/ImageScreen';

const headerTitleStyle = { fontFamily: NeoMedium, fontSize: 18, marginLeft: 0 }

class NavigationRouter extends Component {
    handleBack = () => {
        let screen = Actions.currentScene;
        switch (screen) {
            case 'eventList':
                BackHandler.exitApp();
                return true;
            case 'splash':
                BackHandler.exitApp();
                return true;
            case 'login':
                BackHandler.exitApp();
                return true;
            default:
                Actions.pop();
                return true;
        }
    }

    renderLeftTitle = (title) => () => (
        <View style={{ flex: 1, marginLeft: 20 }}>
            <Text style={{ fontFamily: NeoMedium, fontSize: 18 }}>{title}</Text>
        </View>
    )

    render() {
        const MyTransitionSpec = ({
            duration: 250,
            easing: Easing.bezier(0.2833, 0.99, 0.31833, 0.99),
            // timing: Animated.timing,
        });

        const transitionConfig = () => ({
            transitionSpec: MyTransitionSpec,
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const { index } = scene;
                const width = layout.initWidth;

                // right to left by replacing bottom scene
                return {
                    transform: [{
                        translateX: position.interpolate({
                            inputRange: [index - 1, index, index + 1],
                            outputRange: [width, 0, -width],
                        }),
                    }]
                };
            }
        });

        const headerSetting = (
            <TouchableOpacity
                style={{ height: '100%', aspectRatio: 1, alignItems: 'center', justifyContent: 'center', marginRight: 5 }}
                onPress={() => Actions.setting()}>
                <FontAwesome
                    size={28}
                    color={BlackColor}
                    name={`cog`}
                />
            </TouchableOpacity>
        )

        const noShadowStyle = {
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 1,
            },
            shadowOpacity: 0,
            shadowRadius: 1.00,

            elevation: 0
        }

        return (
            <Router
                backAndroidHandler={this.handleBack}>
                <Stack
                    navigationBarStyle={{ backgroundColor: multiTheme[this.props.chosenTheme].backgroundColor }}
                    transitionConfig={transitionConfig}
                    key='root'>
                    <Scene key='splash'
                        initial
                        hideNavBar
                        component={SplashScreen} />
                    <Scene key='login'
                        hideNavBar
                        component={LoginScreen} />
                    <Scene key='setting'
                        back
                        title='Setting'
                        titleStyle={headerTitleStyle}
                        component={SettingScreen} />
                    <Scene key='eventList'
                        renderBackButton={this.renderLeftTitle('Event list')}
                        renderRightButton={headerSetting}
                        component={EventListScreen} />
                    <Scene key='detailEvent'
                        back
                        title='Detail event'
                        titleStyle={headerTitleStyle}
                        navigationBarStyle={noShadowStyle}
                        component={DetailEventScreen} />
                    <Scene key='scanMenu'
                        back
                        title='Scan menu'
                        titleStyle={headerTitleStyle}
                        component={ScanMenuScreen} />
                    <Scene key='scanList'
                        back
                        navigationBarStyle={noShadowStyle}
                        title='Participant list'
                        titleStyle={headerTitleStyle}
                        component={ScanListScreen} />
                    <Scene key='scanQR'
                        back
                        hideNavBar
                        title='Scan QR'
                        titleStyle={headerTitleStyle}
                        component={ScanQRScreen} />
                    <Scene key='image'
                        back
                        hideNavBar
                        component={ImageScreen} />
                </Stack>
            </Router>
        );
    }
}
const mapStateToProps = state => {
    return {
        chosenLanguage: state.counterOperation.language,
        chosenTheme: state.counterOperation.theme
    };
};


export default connect(mapStateToProps)(NavigationRouter);
