import ACTION_TYPES from "../actions";
import { Actions } from "react-native-router-flux";

const initialState = {
  language: 'EN',
  theme: 'Light'
};

const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPES.CHANGE_LANGUAGE:
      Actions.refresh()
      return {
        ...state,
        language: action.language
      };
    case ACTION_TYPES.CHANGE_THEME:
      Actions.refresh()
      return {
        ...state,
        theme: action.theme
      };
    default:
      return state;
  }
};

export default counterReducer;