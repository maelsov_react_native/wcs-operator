import ACTION_TYPES from '.';

export const changeLanguage = to => {
    return {
        type: ACTION_TYPES.CHANGE_LANGUAGE,
        language: to
    };
};

export const changeTheme = to => {
    return {
        type: ACTION_TYPES.CHANGE_THEME,
        theme: to
    };
};

// export const clear = ({
//     type: ACTION_TYPES.CLEAR
// })