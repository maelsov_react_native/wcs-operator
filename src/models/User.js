import { APILogin, APIGetProfile, APILogout } from '../APIConfig'
import fetchNoCache from '../libraries/fetchNoCache'

export const modelUser = {
    login: (post_data, update) => {
        return fetchNoCache(update, APILogin, 'POST', false, post_data)
    },
    getProfile: (user_id, update) => {
        var params_data = `user_id=${user_id}`
        return fetchNoCache(update, APIGetProfile, 'GET', true, '', params_data)
    },
    logout: (post_data, update) => {
        return fetchNoCache(update, APILogout, 'POST', false, post_data)
    },
}