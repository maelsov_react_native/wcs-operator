import {
    APIEventView,
    APIEventDetail,
    APIAttendance,
    APIScanAttendance,
    APIChangeStatusManual
} from '../APIConfig'
import fetchNoCache from '../libraries/fetchNoCache'

export const modelEvent = {
    getEventList: (limit, offset, update) => {
        var params_data = 'limit=' + limit + '&offset=' + offset;
        return fetchNoCache(update, APIEventView, 'GET', false, null, params_data)
    },
    getEventDetail: (event_id, update) => {
        const params_data = 'event_id=' + event_id;
        return fetchNoCache(update, APIEventDetail, 'GET', false, null, params_data)
    },
    getEventAttendanceList: (event_id, update) => {
        const params_data = 'event_id=' + event_id;
        return fetchNoCache(update, APIAttendance, 'GET', false, null, params_data)
    },
    scanAttendance: (formdata, update) => {
        return fetchNoCache(update, APIScanAttendance, 'POST', true, formdata)
    },
    changeStatusManual: (formdata, update) => {
        return fetchNoCache(update, APIChangeStatusManual, 'POST', true, formdata)
    }
}