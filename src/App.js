import React, { Component } from 'react';
import { Provider } from "react-redux";
import configureStore from "./redux/stores/store";
import RootComponent from './RootComponent';

const store = configureStore();

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RootComponent />
      </Provider>
    );
  }
}
