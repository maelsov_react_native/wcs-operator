const APIRoot = "https://wcsglobal.id/api/v_1/"
const APIRootDev = "https://maelsov.id/wcs_website/api/v_1/"

const ActiveAPI = APIRoot

// API List
export const APILogin = `${ActiveAPI}auth/login`

export const APILogout = `${ActiveAPI}auth/logout`

export const APIGetProfile = `${ActiveAPI}user/profile`

export const APIEditProfile = `${ActiveAPI}user/update_profile`

export const APISendMessage = `${ActiveAPI}message/send`

export const APIEventView = `${ActiveAPI}event/view`
export const APIEventDetail = `${ActiveAPI}event/detail`
export const APIAttendance = `${ActiveAPI}event/event_attendance_list`

export const APIScanAttendance = `${ActiveAPI}scan_qr/`
export const APIChangeStatusManual = `${ActiveAPI}scan_qr/change_status_customer`

// Article

