import { WhiteColor, BlackColor, WCSGreenColor } from "./GlobalConfig";

const multiTheme = {
    Light: {
        backgroundColor: WhiteColor,
        btnBackgroundColor: '#c4c4c4',
        borderColor: '#2d2d2d',
        fontColor: '#000',
        subfontColor: '#9f9f9f',
        subBGHeadingColor: '#e3e3e3',//lightCOlor
        subTXTHeadingColor: '#9f9f9f',//Gray COlor

        //misc for more
        radioColor: '#9f9f9f',

        //misc for image
        tintColor: null,
        btn_color: '#fff',
        barStyle: 'dark-content',
        clickableColor : WCSGreenColor,


    },
    Dark: {
        backgroundColor: '#2d2d2d',
        btnBackgroundColor: '#858585',
        NavBarBackgroundColor: '#ffffff',
        fontColor: '#fff',
        subfontColor: '#b5b5b5',
        subBGHeadingColor: '#4f4f4f',
        subTXTHeadingColor: '#b5b5b5',

        //misc for more
        radioColor: '#fff',

        //misc for image
        tintColor: '#fff',
        btn_color: '#fff',
        barStyle: 'light-content',
        clickableColor : WhiteColor,


    }
}

export default multiTheme