import AesCrypto from 'react-native-aes-pack';

const secretKey = 'wcsm109858739012';
const iv = 'wcsm109858739012';

export const wcsmAesEncrypt = (data) => new Promise((resolve) => {
    AesCrypto.encrypt(data, secretKey, iv).then(cipher => {
        console.log("Encrypt = " + cipher);// return a string type cipher
        // return (cipher)
        resolve(cipher)
    }).catch(err => {
        resolve(false)
        console.log(err);
    });
})

export const wcsmAesDecrypt = (data) => new Promise((resolve) => {
    console.log(data)
    AesCrypto.decrypt(data, secretKey, iv).then(plaintxt => {
        console.log("Decrypt = " + plaintxt);// return a string type plaintxt
        resolve(plaintxt)
    }).catch(err => {
        resolve(false)
        console.log(err);
    });
})