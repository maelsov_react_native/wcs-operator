import AsyncStorage from '@react-native-community/async-storage'

export default async (update, API, requestType = 'GET', useToken = false, post_data = null, params_data = null, debugMode = false) => {
    let token
    if (useToken) {
        await AsyncStorage.getItem('token', (err, res) => {
            if (res) {
                token = res
                console.log('token : ' + token)
            }
        })
    }

    return fetch(`${API}${params_data ? `?${params_data}` : ``}`, {
        method: requestType,
        headers: post_data ?
            {
                'Accept': 'multipart/form-data',
                'Content-Type': 'multipart/form-data',
                'X-Api-Key': useToken ? token : ''
            } :
            {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-Api-Key': useToken ? token : ''
            },
        body: post_data ? post_data : ''
    })
        .then((response) => {
            if (debugMode) {
                console.log(response.text())
                console.log({
                    status: response.status,
                    API: `${API}${params_data ? `?${params_data}` : ``}`,
                    token: useToken ? token : '',
                    body: post_data ? post_data : ''
                })
            }
            const statusCode = response.status;
            const res = response.json()
            return Promise.all([statusCode, res])
        })
        .then(([statusCode, result]) => {
            update(
                {
                    "status": statusCode,
                    "result": result,
                    "isCache": false
                }
            )
        })
        .catch(err => { throw err })

}