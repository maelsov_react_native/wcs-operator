import AsyncStorage from '@react-native-community/async-storage'

export default async (update, API, requestType = 'GET', useToken = false, storageId, post_data = null, params_data = null) => {
    let token
    if (useToken) {
        await AsyncStorage.getItem('token', (err, res) => {
            if (res) {
                token = res
                console.log('token : ' + token)
            }
        })
    }

    // If the data cache is available, update with the cached data first
    await AsyncStorage.getItem(storageId, (error, result) => {
        if (result) {
            result = JSON.parse(result)
            update({
                "isCache": true,
                "result": result
            })
        }
    })

    // Request the latest data from the server, if successful, update the view and update the cache
    return fetch(`${API}${params_data ? `?${params_data}` : ``}`, {
        method: requestType,
        headers: post_data ?
            {
                'Accept': 'multipart/form-data',
                'Content-Type': 'multipart/form-data',
                'X-Api-Key': useToken ? token : ''
            } :
            {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-Api-Key': useToken ? token : ''
            },
        body: post_data ? post_data : ''
    })
        .then((response) => {
            const statusCode = response.status;
            // console.warn(response)
            const res = response.json()
            return Promise.all([statusCode, res])
        })
        .then(([statusCode, result]) => {
            update(
                {
                    "status": statusCode,
                    "result": result,
                    "isCache": false,
                    "url": `${API}${params_data ? `?${params_data}` : ''}`
                }
            )
            if (statusCode == 200) {
                AsyncStorage.setItem(storageId, JSON.stringify(result))
            }
        })
        .catch(err => { throw err })

}